<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMotorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('motors', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id'); 
            $table->integer('picture_id')->unsigned();
            $table->string('year')->nullable();
            $table->string('year_comment')->nullable();
            $table->string('ordinal_number')->nullable();
            $table->date('date')->nullable();
            $table->integer('person_id')->unsigned();
            $table->string('name_comment')->nullable();
            $table->string('machine_type')->nullable();
            $table->string('type')->nullable();
            $table->string('type_comment')->nullable();
            $table->string('serial_number')->nullable(); 
            $table->string('serial_comment')->nullable();
            $table->string('billing')->nullable();
            $table->integer('total')->nullable();
            $table->string('total_comment')->nullable();
            $table->string('repair')->nullable();
            $table->text('repair_comment')->nullable();
            $table->integer('earnings')->nullable();
            $table->string('earnings_comment')->nullable();
            $table->integer('expense')->nullable();
            $table->string('expense_comment')->nullable();
            $table->string('iron_core_lenght')->nullable();
            $table->string('iron_core_lenght_comment')->nullable();
            $table->string('iron_core_diameter')->nullable();
            $table->string('iron_core_diameter_comment')->nullable();
            $table->string('swivel')->nullable();
            $table->string('kw')->nullable();
            $table->string('kw_comment')->nullable();
            $table->string('pole')->nullable(); // eddig
            $table->string('pole_comment')->nullable();
            $table->string('volt')->nullable();
            $table->string('volt_comment')->nullable();
            $table->string('amper')->nullable();
            $table->string('amper_comment')->nullable();
            $table->string('stria')->nullable();
            $table->integer('relay')->nullable();
            $table->string('relay_comment')->nullable();
            $table->integer('condenser')->nullable();
            $table->string('condenser_comment')->nullable();
            $table->integer('anchor')->nullable();
            $table->string('anchor_comment')->nullable();
            $table->integer('stator')->nullable();
            $table->string('stator_comment')->nullable();
            $table->integer('brake')->nullable();
            $table->integer('is_there_serial_number')->nullable();
            $table->string('is_there_serial_number_comment')->nullable();
            $table->string('y_or_d')->nullable();
            $table->string('y_or_d_comment')->nullable();
            $table->string('wire')->nullable();
            $table->string('wire_comment')->nullable();
            $table->integer('turning')->nullable();
            $table->string('turning_comment')->nullable();
            $table->integer('component')->nullable();
            $table->string('component_comment')->nullable();
            $table->integer('envelope')->nullable();
            $table->string('pattern')->nullable();
            $table->string('pattern_comment')->nullable();
            $table->integer('pillow')->nullable();
            $table->string('pillow_comment')->nullable();
            $table->integer('seal')->nullable();
            $table->string('seal_comment')->nullable();
            $table->integer('slider_ring')->nullable();
            $table->string('slider_ring_comment')->nullable();
            $table->integer('coal_brush')->nullable();
            $table->string('coal_brush_comment')->nullable();
            $table->integer('is_there_draw')->nullable();
            $table->string('is_there_draw_comment')->nullable();
            $table->string('kg')->nullable();
            $table->string('kg_comment')->nullable();
            $table->integer('primary_phase_resistor')->nullable();
            $table->string('primary_phase_resistor_comment')->nullable();
            $table->integer('secondary_phase_resistor')->nullable();
            $table->string('secondary_phase_resistor_comment')->nullable();
            $table->integer('empty_power')->nullable();
            $table->string('empty_power_comment')->nullable();
            $table->integer('standard_time')->nullable();
            $table->string('standard_time_comment')->nullable();
            $table->timestamps();            
        });
        Schema::table('motors', function(Blueprint $table) {
            $table->foreign('person_id')->references('id')->on('persons')->onDelete('cascade');    
        });
        Schema::table('motors', function(Blueprint $table) {
            $table->foreign('picture_id')->references('picture_id')->on('pictures');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('motors');
    }
}