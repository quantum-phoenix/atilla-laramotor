<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePicturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pictures', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('picture_id')->unsigned();
            $table->index('picture_id');
            $table->string('picture_name')->nullable();
            $table->string('picture_file_name');
            $table->timestamps();            
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pictures');
    }
}
