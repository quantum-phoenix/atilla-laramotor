$(document).ready(function() {
    // $.getJSON("search/autocomplete", function(data){
    //     //console.log(JSON.stringify(data));
    //     //var obj = $.parseJSON(data);
    //     //data = JSON.parse(data);
    //     $.each(data, function(key, val){
    //         console.log(val.name);
    //     });
    // });
    // var jqxhr = $.getJSON( "search/autocomplete.json", function(data) {
    //     get_json(data);
    //     console.log( "success" );
    // })
    // .done(function() {
    //     console.log( "second success" );
    // })
    // .fail(function() {
    //     console.log( "error" );
    // })
    // .always(function() {
    //     console.log( "complete" );
    // });
    // function tryParseJSON (jsonString){
    //     try {
    //         var o = JSON.parse(jsonString);
    //         // Handle non-exception-throwing cases:
    //         // Neither JSON.parse(false) or JSON.parse(1234) throw errors, hence the type-checking,
    //         // but... JSON.parse(null) returns 'null', and typeof null === "object", 
    //         // so we must check for that, too.
    //         if (o && typeof o === "object" && o !== null) {
    //             return o;
    //         }
    //     }
    //     catch (e) { 
    //         console.log("Hiba: "+e+" | o: "+o);
    //     }
    //     return false;
    // };
    // function get_json(txt)
    // {  var data
    //    try     {  data = eval('('+txt+')'); }
    //    catch(e){  data = false;             }
    //    return data;
    // }
    // // Perform other work here ...
    // // Set another completion function for the request above
    // jqxhr.complete(function() {
    //   console.log( "second complete" );
    // });
    // var options = {
    //     url: "search/autocomplete",
    //     getValue: "name",
    //     list: {
    //         match: {
    //             enabled: true
    //         }
    //     },
    //     theme: "plate-dark"
    // };
    // $("#name").easyAutocomplete(options);
    // var projects = [
    //   {
    //     value: "jquery",
    //     label: "jQuery",
    //     desc: "the write less, do more, JavaScript library",
    //     icon: "jquery_32x32.png"
    //   },
    //   {
    //     value: "jquery-ui",
    //     label: "jQuery UI",
    //     desc: "the official user interface library for jQuery",
    //     icon: "jqueryui_32x32.png"
    //   },
    //   {
    //     value: "sizzlejs",
    //     label: "Sizzle JS",
    //     desc: "a pure-JavaScript CSS selector engine",
    //     icon: "sizzlejs_32x32.png"
    //   }
    // ];
    // $( "#name" ).autocomplete({
    //     source: "search/autocomplete",
    //     //source: projects,
    //     minLength: 1,
    //     select: function(event, ui) {
    //         console.log(ui.item);
    //         $('#name').val(ui.item.name);
    //         return false;
    //     }
    // });
    $("#name").autocomplete({
        source: function(request, response) {
            $.ajax({
                url: "/search/autocomplete/name",
                data: {
                    name: this.term
                },
                success: function(data, textStatus, jqXHR) {
                    console.log("[SUCCESS] " + data.length + " item(s)");
                    response($.map(data, function(c) {
                        return {
                            label: c.name,
                            value: c.name
                        }
                    }));
                    //response(data); //Na igy már nem jó
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log("[ERROR] - Ajax error kivétel");
                    response([]);
                }
            });
        },
        minLength: 2,
        select: function(event, ui) {
            $('#name').val(ui.item.label);
            return false;
        }
    });

    $('.has_tooltip').each(function() { // Notice the .each() loop, discussed below
        $(this).qtip({
            content: {
                text: $(this).next('div.tooltip_content') // Use the "div" element next to this for the content
            }
        });
    });

    $(".fancybox").fancybox({
        helpers: {
          title : {
              type : 'float'
          }
        }
    });

});