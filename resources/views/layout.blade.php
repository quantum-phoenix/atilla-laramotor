<!DOCTYPE html>
<html>
    <head>
        <title>LaraMotor</title>
        <meta charset="UTF-8">
        <!--[if lt IE 9]><script src="dependencies/respond.js"></script><!--<![endif]-->
        <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/css/bootstrap-theme.min.css">
        <link rel="stylesheet" type="text/css" href="/css/easy-autocomplete.min.css">        
        <link rel="stylesheet" type="text/css" href="/css/jquery-ui.min.css">
        <link rel="stylesheet" type="text/css" href="/css/jquery.qtip.min.css">     
        <link rel="stylesheet" type="text/css" href="/css/jquery.fancybox.css">
        <link rel="stylesheet" type="text/css" href="/css/theme.css">
    </head>
    <body>
        <div class="container">
            <div class="content">
                @yield('content')
            </div>			
        </div>
    </body>
    <script type="text/javascript" src="/js/jquery.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/jquery.easy-autocomplete.min.js"></script>
    <script type="text/javascript" src="/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="/js/jquery.qtip.min.js"></script>
    <script type="text/javascript" src="/js/jquery.fancybox.js"></script>
    <script type="text/javascript" src="/js/main.js"></script>
</html>
