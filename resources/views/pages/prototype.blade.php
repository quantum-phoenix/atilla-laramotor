@extends('layout')

@section('content')
<ul>
@foreach ($res as $row)     
    @if (!empty($row->person))
        <li class="list-group-item">
            {!! $row->person->name !!}
        </li>
    @endif
@endforeach
</ul>
@stop