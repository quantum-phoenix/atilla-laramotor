@extends('layout')

@section('content')
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
  			<div class="panel-body">
				<h1>Lara Motor</h1>
				<h4>V{{ $ver }} Frissitve: ({{ $date }})</h4>
				<ul class="list-group">
				    {{--<li class="list-group-item">{!! '<a class="btn btn-default" href="database">Adatbázis</a>' !!}</li>--}}
				    <li class="list-group-item">{!! '<a class="btn btn-default" href="search">Keresés</a>' !!}</li>
				    <li class="list-group-item">{!! '<a class="btn btn-default" href="motors/create">Új bejegyzés</a>' !!}</li>
				    <li class="list-group-item">{!! '<a class="btn btn-default" href="persons">Emberek</a>' !!}</li>
				</ul>
				@if(Session::has('success'))
					<div class="alert alert-success" role="alert">
						<p>{!! Session::get('success') !!}</p>
					</div>
				@endif
			</div>
		</div>
	</div>
</div>
@stop