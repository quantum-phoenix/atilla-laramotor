@extends('layout')

@section('content')

<div class="row">
	<div class="col-lg-12">
		<a class="btn btn-default" href="/">Vissza</a>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<div class="white-box table-responsive table-condensed table-hover table-bordered">
			<table class="table table-bordered">
			    <thead>
			        <tr>
			        	<th>{{ $lang['id'] }}</th>
			            <th>{{ $lang['year_comment'] }}</th>
			            <th>{{ $lang['ordinal_number'] }}</th>
			            <th>{{ $lang['date'] }}</th>
			            <th>{{ $lang['name'] }}</th>
			            <th>{{ $lang['name_comment'] }}</th>
			            <th>{{ $lang['machine_type'] }}</th>
			            <th>{{ $lang['type'] }}</th>
			            <th>{{ $lang['type_comment'] }}</th>
			            <th>{{ $lang['serial_number'] }}</th>
			            <th>{{ $lang['serial_comment'] }}</th>
			            <th>{{ $lang['billing'] }}</th>
			            <th>{{ $lang['total'] }}</th>
			            <th>{{ $lang['total_comment'] }}</th>
			            <th>{{ $lang['repair'] }}</th>
			            <th>{{ $lang['repair_comment'] }}</th>
			            <th>{{ $lang['earnings'] }}</th>
			            <th>{{ $lang['earnings_comment'] }}</th>
			            <th>{{ $lang['expense'] }}</th>
			            <th>{{ $lang['expense_comment'] }}</th>
			            <th>{{ $lang['iron_core_lenght'] }}</th>
			            <th>{{ $lang['iron_core_lenght_comment'] }}</th>
			            <th>{{ $lang['iron_core_diameter'] }}</th>
			            <th>{{ $lang['iron_core_diameter_comment'] }}</th>
			            <th>{{ $lang['swivel'] }}</th>
			            <th>{{ $lang['kw'] }}</th>
			            <th>{{ $lang['kw_comment'] }}</th>
			            <th>{{ $lang['pole'] }}</th>
			            <th>{{ $lang['pole_comment'] }}</th>
			            <th>{{ $lang['volt'] }}</th>
			            <th>{{ $lang['volt_comment'] }}</th>
			            <th>{{ $lang['amper'] }}</th>
			            <th>{{ $lang['amper_comment'] }}</th>
			            <th>{{ $lang['stria'] }}</th>
			            <th>{{ $lang['relay'] }}</th>
			            <th>{{ $lang['relay_comment'] }}</th>
			            <th>{{ $lang['condenser'] }}</th>
			            <th>{{ $lang['condenser_comment'] }}</th>
			            <th>{{ $lang['anchor'] }}</th>
			            <th>{{ $lang['anchor_comment'] }}</th>
			            <th>{{ $lang['stator'] }}</th>
			            <th>{{ $lang['stator_comment'] }}</th>
			            <th>{{ $lang['brake'] }}</th>
			            <th>{{ $lang['is_there_serial_number'] }}</th>
			            <th>{{ $lang['is_there_serial_number_comment'] }}</th>
			            <th>{{ $lang['y_or_d'] }}</th>
			            <th>{{ $lang['y_or_d_comment'] }}</th>
			            <th>{{ $lang['wire'] }}</th>
			            <th>{{ $lang['wire_comment'] }}</th>
			            <th>{{ $lang['turning'] }}</th>
			            <th>{{ $lang['turning_comment'] }}</th>
			            <th>{{ $lang['component'] }}</th>
			            <th>{{ $lang['component_comment'] }}</th>
			            <th>{{ $lang['envelope'] }}</th>
			            <th>{{ $lang['pattern'] }}</th>
			            <th>{{ $lang['pattern_comment'] }}</th>
			            <th>{{ $lang['pillow'] }}</th>
			            <th>{{ $lang['pillow_comment'] }}</th>
			            <th>{{ $lang['seal'] }}</th>
			            <th>{{ $lang['seal_comment'] }}</th>
			            <th>{{ $lang['slider_ring'] }}</th>
			            <th>{{ $lang['slider_ring_comment'] }}</th>
			            <th>{{ $lang['coal_brush'] }}</th>
			            <th>{{ $lang['coal_brush_comment'] }}</th>
			            <th>{{ $lang['is_there_draw'] }}</th>
			            <th>{{ $lang['is_there_draw_comment'] }}</th>
			            <th>{{ $lang['kg'] }}</th>
			            <th>{{ $lang['kg_comment'] }}</th>
			            <th>{{ $lang['primary_phase_resistor'] }}</th>
			            <th>{{ $lang['primary_phase_resistor_comment'] }}</th>
			            <th>{{ $lang['secondary_phase_resistor'] }}</th>
			            <th>{{ $lang['secondary_phase_resistor_comment'] }}</th>
			            <th>{{ $lang['empty_power'] }}</th>
			            <th>{{ $lang['empty_power_comment'] }}</th>
			            <th>{{ $lang['standard_time'] }}</th>
			            <th>{{ $lang['standard_time_comment'] }}</th>
			            <th>{{ $lang['image'] }}</th>
			            <th>{{ $lang['view'] }}</th>
			        </tr>
			    </thead>
			    <tbody>
				@foreach ($motors as $motor)
			        <tr>
			        	<td>{{ $motor->id }}</td>
			            <td>{{ $motor->year_comment }}</td>
			            <td>{{ $motor->ordinal_number }}</td>
			            <td>{!! date('Y m', strtotime($motor->date)) !!}</td>
			            <td>{{ $motor->name }}</td>
			            <td>{{ $motor->name_comment }}</td>
			            <td>{{ $motor->machine_type }}</td>
			            <td>{{ $motor->type }}</td>
			            <td>{{ $motor->type_comment }}</td>
			            <td>{{ $motor->serial_number }}</td>
			            <td>{{ $motor->serial_comment }}</td>
			            <td>{{ $motor->billing }}</td>
			            <td>{{ $motor->total }}</td>
			            <td>{{ $motor->total_comment }}</td>
			            <td>{{ $motor->repair }}</td>
			            <td>{!! nl2br(e($motor->repair_comment)) !!}</td>
			            <td>{{ $motor->earnings }}</td>
			            <td>{{ $motor->earnings_comment }}</td>
			            <td>{{ $motor->expense }}</td>
			            <td>{{ $motor->expense_comment }}</td>
			            <td>{{ $motor->iron_core_lenght }}</td>
			            <td>{{ $motor->iron_core_lenght_comment }}</td>
			            <td>{{ $motor->iron_core_diameter }}</td>
			            <td>{{ $motor->iron_core_diameter_comment }}</td>
			            <td>{{ $motor->swivel }}</td>
			            <td>{{ $motor->kw }}</td>
			            <td>{{ $motor->kw_comment }}</td>
			            <td>{{ $motor->pole }}</td>
			            <td>{{ $motor->pole_comment }}</td>
			            <td>{{ $motor->volt }}</td>
			            <td>{{ $motor->volt_comment }}</td>
			            <td>{{ $motor->amper }}</td>
			            <td>{{ $motor->amper_comment }}</td>
			            <td>{{ $motor->stria }}</td>
			            <td>{{ $motor->relay }}</td>
			            <td>{{ $motor->relay_comment }}</td>
			            <td>{{ $motor->condenser }}</td>
			            <td>{{ $motor->condenser_comment }}</td>
			            <td>{{ $motor->anchor }}</td>
			            <td>{{ $motor->anchor_comment }}</td>
			            <td>{{ $motor->stator }}</td>
			            <td>{{ $motor->stator_comment }}</td>
			            <td>{{ $motor->brake }}</td>
			            <td>{{ $motor->is_there_serial_number }}</td>
			            <td>{{ $motor->is_there_serial_number_comment }}</td>
			            <td>{{ $motor->y_or_d }}</td>
			            <td>{{ $motor->y_or_d_comment }}</td>
			            <td>{{ $motor->wire }}</td>
			            <td>{{ $motor->wire_comment }}</td>
			            <td>{{ $motor->turning }}</td>
			            <td>{{ $motor->turning_comment }}</td>
			            <td>{{ $motor->component }}</td>
			            <td>{{ $motor->component_comment }}</td>
			            <td>{{ $motor->envelope }}</td>
			            <td>{{ $motor->pattern }}</td>
			            <td>{{ $motor->pattern_comment }}</td>
			            <td>{{ $motor->pillow }}</td>
			            <td>{{ $motor->pillow_comment }}</td>
			            <td>{{ $motor->seal }}</td>
			            <td>{{ $motor->seal_comment }}</td>
			            <td>{{ $motor->slider_ring }}</td>
			            <td>{{ $motor->slider_ring_comment }}</td>
			            <td>{{ $motor->coal_brush }}</td>
			            <td>{{ $motor->coal_brush_comment }}</td>
			            <td>{{ $motor->is_there_draw }}</td>
			            <td>{{ $motor->is_there_draw_comment }}</td>
			            <td>{{ $motor->kg }}</td>
			            <td>{{ $motor->kg_comment }}</td>
			            <td>{{ $motor->primary_phase_resistor }}</td>
			            <td>{{ $motor->primary_phase_resistor_comment }}</td>
			            <td>{{ $motor->secondary_phase_resistor }}</td>
			            <td>{{ $motor->secondary_phase_resistor_comment }}</td>
			            <td>{{ $motor->empty_power }}</td>
			            <td>{{ $motor->empty_power_comment }}</td>
			            <td>{{ $motor->standard_time }}</td>
			            <td>{{ $motor->standard_time_comment }}</td>
			            <td>
                            @if(!empty($motor->image))
                                <a class="btn btn-link btn-xs fancybox" href="/uploads/{{ $motor->image }}" title="Kép">Kép</a> 
                            @else
                                Nincs kép
                            @endif                                               
                        </td>
			            <td><a class="btn btn-link" href="/motors/{{ $motor->id }}">Megnéz</a></td>
			        </tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<div class="found-box white-box">
				@if (!empty($count))  
					<p>{{ $count }} sort találtam.</p>
				@endif	
		</div>
	</div>
</div>

<div class="row">
    <div class="col-lg-12">
        {!! $motors->appends(Input::all())->render() !!}
    </div>
</div>	

@stop