@extends('layout')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <a class="btn btn-default" href="/search">Vissza</a>
    </div>
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
  			<div class="panel-body">
  			<h1>Szerkesztés</h1>
			{!! Form::model($motors, array('route' => array('motors.update', $motors->id), 'method' => 'PATCH')) !!}
				<div class="form-group">
				    {!! Form::label('year', $lang['year']) !!}
				    {!! Form::text('year', Input::old('year'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('year_comment', $lang['year_comment']) !!}
				    {!! Form::text('year_comment', Input::old('year_comment'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('date', $lang['date']) !!}
				    {!! Form::text('date', Input::old('date'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('name', $lang['name']) !!}
				    {!! Form::text('name', Input::old('name'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('name_comment', $lang['name_comment']) !!}
				    {!! Form::text('name_comment', Input::old('name_comment'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('machine_type', $lang['machine_type']) !!}
				    {!! Form::text('machine_type', Input::old('machine_type'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('type', $lang['type']) !!}
				    {!! Form::text('type', Input::old('type'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('type_comment', $lang['type_comment']) !!}
				    {!! Form::text('type_comment', Input::old('type_comment'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('serial_number', $lang['serial_number']) !!}
				    {!! Form::text('serial_number', Input::old('serial_number'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('serial_comment', $lang['serial_comment']) !!}
				    {!! Form::text('serial_comment', Input::old('serial_comment'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('billing', $lang['billing']) !!}
				    {!! Form::text('billing', Input::old('billing'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('total', $lang['total']) !!}
				    {!! Form::text('total', Input::old('total'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('total_comment', $lang['total_comment']) !!}
				    {!! Form::text('total_comment', Input::old('total_comment'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('repair', $lang['repair']) !!}
				    {!! Form::text('repair', Input::old('repair'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('repair_comment', $lang['repair_comment']) !!}
				    {!! Form::textarea('repair_comment', Input::old('repair_comment'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('earnings', $lang['earnings']) !!}
				    {!! Form::text('earnings', Input::old('earnings'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('earnings_comment', $lang['earnings_comment']) !!}
				    {!! Form::text('earnings_comment', Input::old('earnings_comment'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('expense', $lang['expense']) !!}
				    {!! Form::text('expense', Input::old('expense'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('expense_comment', $lang['expense_comment']) !!}
				    {!! Form::text('expense_comment', Input::old('expense_comment'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('iron_core_lenght', $lang['iron_core_lenght']) !!}
				    {!! Form::text('iron_core_lenght', Input::old('iron_core_lenght'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('iron_core_lenght_comment', $lang['iron_core_lenght_comment']) !!}
				    {!! Form::text('iron_core_lenght_comment', Input::old('iron_core_lenght_comment'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('iron_core_diameter', $lang['iron_core_diameter']) !!}
				    {!! Form::text('iron_core_diameter', Input::old('iron_core_diameter'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('iron_core_diameter_comment', $lang['iron_core_diameter_comment']) !!}
				    {!! Form::text('iron_core_diameter_comment', Input::old('iron_core_diameter_comment'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('swivel', $lang['swivel']) !!}
				    {!! Form::text('swivel', Input::old('swivel'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('kw', $lang['kw']) !!}
				    {!! Form::text('kw', Input::old('kw'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('kw_comment', $lang['kw_comment']) !!}
				    {!! Form::text('kw_comment', Input::old('kw_comment'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('pole', $lang['pole']) !!}
				    {!! Form::text('pole', Input::old('pole'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('pole_comment', $lang['pole_comment']) !!}
				    {!! Form::text('pole_comment', Input::old('pole_comment'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('volt', $lang['volt']) !!}
				    {!! Form::text('volt', Input::old('volt'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('volt_comment', $lang['volt_comment']) !!}
				    {!! Form::text('volt_comment', Input::old('volt_comment'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('amper', $lang['amper']) !!}
				    {!! Form::text('amper', Input::old('amper'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('amper_comment', $lang['amper_comment']) !!}
				    {!! Form::text('amper_comment', Input::old('amper_comment'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('stria', $lang['stria']) !!}
				    {!! Form::text('stria', Input::old('stria'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('relay', $lang['relay']) !!}
				    {!! Form::text('relay', Input::old('relay'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('relay_comment', $lang['relay_comment']) !!}
				    {!! Form::text('relay_comment', Input::old('relay_comment'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('condenser', $lang['condenser']) !!}
				    {!! Form::text('condenser', Input::old('condenser'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('condenser_comment', $lang['condenser_comment']) !!}
				    {!! Form::text('condenser_comment', Input::old('condenser_comment'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('anchor', $lang['anchor']) !!}
				    {!! Form::text('anchor', Input::old('anchor'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('anchor_comment', $lang['anchor_comment']) !!}
				    {!! Form::text('anchor_comment', Input::old('anchor_comment'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('stator', $lang['stator']) !!}
				    {!! Form::text('stator', Input::old('stator'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('stator_comment', $lang['stator_comment']) !!}
				    {!! Form::text('stator_comment', Input::old('stator_comment'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('brake', $lang['brake']) !!}
				    {!! Form::text('brake', Input::old('brake'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('is_there_serial_number', $lang['is_there_serial_number']) !!}
				    {!! Form::text('is_there_serial_number', Input::old('is_there_serial_number'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('is_there_serial_number_comment', $lang['is_there_serial_number_comment']) !!}
				    {!! Form::text('is_there_serial_number_comment', Input::old('is_there_serial_number_comment'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('y_or_d', $lang['y_or_d']) !!}
				    {!! Form::text('y_or_d', Input::old('y_or_d'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('y_or_d_comment', $lang['y_or_d_comment']) !!}
				    {!! Form::text('y_or_d_comment', Input::old('y_or_d_comment'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('wire', $lang['wire']) !!}
				    {!! Form::text('wire', Input::old('wire'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('wire_comment', $lang['wire_comment']) !!}
				    {!! Form::text('wire_comment', Input::old('wire_comment'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('turning', $lang['turning']) !!}
				    {!! Form::text('turning', Input::old('turning'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('turning_comment', $lang['turning_comment']) !!}
				    {!! Form::text('turning_comment', Input::old('turning_comment'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('component', $lang['component']) !!}
				    {!! Form::text('component', Input::old('component'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('component_comment', $lang['component_comment']) !!}
				    {!! Form::text('component_comment', Input::old('component_comment'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('envelope', $lang['envelope']) !!}
				    {!! Form::text('envelope', Input::old('envelope'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('pattern', $lang['pattern']) !!}
				    {!! Form::text('pattern', Input::old('pattern'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('pattern_comment', $lang['pattern_comment']) !!}
				    {!! Form::text('pattern_comment', Input::old('pattern_comment'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('pillow', $lang['pillow']) !!}
				    {!! Form::text('pillow', Input::old('pillow'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('pillow_comment', $lang['pillow_comment']) !!}
				    {!! Form::text('pillow_comment', Input::old('pillow_comment'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('seal', $lang['seal']) !!}
				    {!! Form::text('seal', Input::old('seal'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('seal_comment', $lang['seal_comment']) !!}
				    {!! Form::text('seal_comment', Input::old('seal_comment'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('slider_ring', $lang['slider_ring']) !!}
				    {!! Form::text('slider_ring', Input::old('slider_ring'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('slider_ring_comment', $lang['slider_ring_comment']) !!}
				    {!! Form::text('slider_ring_comment', Input::old('slider_ring_comment'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('coal_brush', $lang['coal_brush']) !!}
				    {!! Form::text('coal_brush', Input::old('coal_brush'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('coal_brush_comment', $lang['coal_brush_comment']) !!}
				    {!! Form::text('coal_brush_comment', Input::old('coal_brush_comment'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('is_there_draw', $lang['is_there_draw']) !!}
				    {!! Form::text('is_there_draw', Input::old('is_there_draw'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('is_there_draw_comment', $lang['is_there_draw_comment']) !!}
				    {!! Form::text('is_there_draw_comment', Input::old('is_there_draw_comment'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('kg', $lang['kg']) !!}
				    {!! Form::text('kg', Input::old('kg'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('kg_comment', $lang['kg_comment']) !!}
				    {!! Form::text('kg_comment', Input::old('kg_comment'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('primary_phase_resistor', $lang['primary_phase_resistor']) !!}
				    {!! Form::text('primary_phase_resistor', Input::old('primary_phase_resistor'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('primary_phase_resistor_comment', $lang['primary_phase_resistor_comment']) !!}
				    {!! Form::text('primary_phase_resistor_comment', Input::old('primary_phase_resistor_comment'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('secondary_phase_resistor', $lang['secondary_phase_resistor']) !!}
				    {!! Form::text('secondary_phase_resistor', Input::old('secondary_phase_resistor'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('secondary_phase_resistor_comment', $lang['secondary_phase_resistor_comment']) !!}
				    {!! Form::text('secondary_phase_resistor_comment', Input::old('secondary_phase_resistor_comment'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('empty_power', $lang['empty_power']) !!}
				    {!! Form::text('empty_power', Input::old('empty_power'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('empty_power_comment', $lang['empty_power_comment']) !!}
				    {!! Form::text('empty_power_comment', Input::old('empty_power_comment'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('standard_time', $lang['standard_time']) !!}
				    {!! Form::text('standard_time', Input::old('standard_time'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('standard_time_comment', $lang['standard_time_comment']) !!}
				    {!! Form::text('standard_time_comment', Input::old('standard_time_comment'), array('class' => 'form-control')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('view', $lang['view']) !!}
				    {!! Form::text('view', Input::old('view'), array('class' => 'form-control')) !!}
				</div>
				@if(Session::has('success'))
					<div class="alert alert-success" role="alert">
						<p>{!! Session::get('success') !!}</p>
					</div>
				@endif
				{!! Form::submit('Szerkesztés', array('class' => 'btn btn-default')) !!}
			{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@stop