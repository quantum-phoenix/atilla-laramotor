@extends('layout')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <a class="btn btn-default" href="/">Vissza</a>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
            <h1>Keresés</h1>
            {!! Form::open(array('method' =>'GET','url' => 'found','class' => '')) !!}
                <div class="row">
                    <div class="col-lg-2">
                        <div class="form-group">
                            {!! Form::label('relay', $lang['relay']) !!}
                            <select class="form-control" name="relay">
                                <option value="null">Nem keresem</option>
                                <option value="false">Nincs</option>
                                <option value="true">Van</option>                       
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            {!! Form::label('condenser', $lang['condenser']) !!}
                            <select class="form-control" name="condenser">
                                <option value="null">Nem keresem</option>
                                <option value="false">Nincs</option>
                                <option value="true">Van</option>                       
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            {!! Form::label('anchor', $lang['anchor']) !!}
                            <select class="form-control" name="anchor">
                                <option value="null">Nem keresem</option>
                                <option value="false">Nincs</option>
                                <option value="true">Van</option>                       
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            {!! Form::label('stator', $lang['stator']) !!}
                            <select class="form-control" name="stator">
                                <option value="null">Nem keresem</option>
                                <option value="false">Nincs</option>
                                <option value="true">Van</option>                       
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            {!! Form::label('brake', $lang['brake']) !!}
                            <select class="form-control" name="brake">
                                <option value="null">Nem keresem</option>
                                <option value="false">Nincs</option>
                                <option value="true">Van</option>                       
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            {!! Form::label('is_there_serial_number', $lang['is_there_serial_number']) !!}
                            <select class="form-control" name="is_there_serial_number">
                                <option value="null">Nem keresem</option>
                                <option value="false">Nincs</option>
                                <option value="true">Van</option>                       
                            </select>
                        </div>
                    </div>
                </div>    

                <div class="row">
                    <div class="col-lg-2">
                        <div class="form-group">
                            {!! Form::label('turning', $lang['turning']) !!}
                            <select class="form-control" name="turning">
                                <option value="null">Nem keresem</option>
                                <option value="false">Nincs</option>
                                <option value="true">Van</option>                       
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            {!! Form::label('component', $lang['component']) !!}
                            <select class="form-control" name="component">
                                <option value="null">Nem keresem</option>
                                <option value="false">Nincs</option>
                                <option value="true">Van</option>                       
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            {!! Form::label('envelope', $lang['envelope']) !!}
                            <select class="form-control" name="envelope">
                                <option value="null">Nem keresem</option>
                                <option value="false">Nincs</option>
                                <option value="true">Van</option>                       
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            {!! Form::label('pillow', $lang['pillow']) !!}
                            <select class="form-control" name="pillow">
                                <option value="null">Nem keresem</option>
                                <option value="false">Nincs</option>
                                <option value="true">Van</option>                       
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            {!! Form::label('seal', $lang['seal']) !!}
                            <select class="form-control" name="seal">
                                <option value="null">Nem keresem</option>
                                <option value="false">Nincs</option>
                                <option value="true">Van</option>                       
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            {!! Form::label('slider_ring', $lang['slider_ring']) !!}
                            <select class="form-control" name="slider_ring">
                                <option value="null">Nem keresem</option>
                                <option value="false">Nincs</option>
                                <option value="true">Van</option>                       
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-2">
                        <div class="form-group">
                            {!! Form::label('coal_brush', $lang['coal_brush']) !!}
                            <select class="form-control" name="coal_brush">
                                <option value="null">Nem keresem</option>
                                <option value="false">Nincs</option>
                                <option value="true">Van</option>                       
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            {!! Form::label('is_there_draw', $lang['is_there_draw']) !!}
                            <select class="form-control" name="is_there_draw">
                                <option value="null">Nem keresem</option>
                                <option value="false">Nincs</option>
                                <option value="true">Van</option>                       
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            {!! Form::label('primary_phase_resistor', $lang['primary_phase_resistor']) !!}
                            <select class="form-control" name="primary_phase_resistor">
                                <option value="null">Nem keresem</option>
                                <option value="false">Nincs</option>
                                <option value="true">Van</option>                       
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            {!! Form::label('secondary_phase_resistor', $lang['secondary_phase_resistor']) !!}
                            <select class="form-control" name="secondary_phase_resistor">
                                <option value="null">Nem keresem</option>
                                <option value="false">Nincs</option>
                                <option value="true">Van</option>                       
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            {!! Form::label('empty_power', $lang['empty_power']) !!}
                            <select class="form-control" name="empty_power">
                                <option value="null">Nem keresem</option>
                                <option value="false">Nincs</option>
                                <option value="true">Van</option>                       
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            {!! Form::label('standard_time', $lang['standard_time']) !!}
                            <select class="form-control" name="standard_time">
                                <option value="null">Nem keresem</option>
                                <option value="false">Nincs</option>
                                <option value="true">Van</option>                       
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            {!! Form::label('date', $lang['date']) !!}
                            <div class="row">
                                <div class="col-lg-4">
                                    <select class="form-control" name="from_year" readonly>
                                        <option value="2000">2000</option>
                                        <option value="2001">2001</option>
                                        <option value="2002">2002</option>
                                        <option value="2003">2003</option>
                                        <option value="2004">2004</option>
                                        <option value="2005">2005</option>
                                        <option value="2006">2006</option>
                                        <option value="2007">2007</option> 
                                        <option value="2008">2008</option> 
                                        <option value="2009">2009</option> 
                                        <option value="2010">2010</option> 
                                        <option value="2011">2011</option>
                                        <option value="2012">2012</option> 
                                        <option value="2013">2013</option> 
                                        <option value="2014">2014</option>
                                        <option value="2015">2015</option>
                                        <option value="2016">2016</option>                    
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <select class="form-control" name="from_month" readonly>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option> 
                                        <option value="4">4</option> 
                                        <option value="5">5</option> 
                                        <option value="6">6</option> 
                                        <option value="7">7</option> 
                                        <option value="8">8</option> 
                                        <option value="9">9</option> 
                                        <option value="10">10</option>
                                        <option value="11">11</option> 
                                        <option value="12">12</option>                      
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <select class="form-control" name="from_day" readonly>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option> 
                                        <option value="4">4</option> 
                                        <option value="5">5</option> 
                                        <option value="6">6</option> 
                                        <option value="7">7</option> 
                                        <option value="8">8</option> 
                                        <option value="9">9</option> 
                                        <option value="10">10</option>
                                        <option value="11">11</option> 
                                        <option value="12">12</option>
                                        <option value="13">13</option>
                                        <option value="14">14</option>
                                        <option value="15">15</option>
                                        <option value="16">16</option>
                                        <option value="17">17</option>
                                        <option value="18">18</option>
                                        <option value="19">19</option>
                                        <option value="20">20</option>
                                        <option value="21">21</option>
                                        <option value="22">22</option>
                                        <option value="23">23</option>
                                        <option value="24">24</option>
                                        <option value="25">25</option>
                                        <option value="26">26</option>
                                        <option value="27">27</option>
                                        <option value="28">28</option>
                                        <option value="29">29</option>
                                        <option value="30">30</option>
                                        <option value="31">31</option>                  
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <select class="form-control" name="to_year" readonly>
                                        <option value="2016">2016</option>
                                        <option value="2015">2015</option>
                                        <option value="2014">2014</option> 
                                        <option value="2013">2013</option> 
                                        <option value="2012">2012</option> 
                                        <option value="2011">2011</option> 
                                        <option value="2010">2010</option>
                                        <option value="2009">2009</option> 
                                        <option value="2008">2008</option> 
                                        <option value="2007">2007</option>
                                        <option value="2006">2006</option>
                                        <option value="2005">2005</option>
                                        <option value="2004">2004</option>
                                        <option value="2003">2003</option>
                                        <option value="2002">2002</option>
                                        <option value="2001">2001</option>
                                        <option value="2000">2000</option>                  
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <select class="form-control" name="to_month" readonly>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option> 
                                        <option value="4">4</option> 
                                        <option value="5">5</option> 
                                        <option value="6">6</option> 
                                        <option value="7">7</option> 
                                        <option value="8">8</option> 
                                        <option value="9">9</option> 
                                        <option value="10">10</option>
                                        <option value="11">11</option> 
                                        <option value="12">12</option>                      
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <select class="form-control" name="to_day" readonly>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option> 
                                        <option value="4">4</option> 
                                        <option value="5">5</option> 
                                        <option value="6">6</option> 
                                        <option value="7">7</option> 
                                        <option value="8">8</option> 
                                        <option value="9">9</option> 
                                        <option value="10">10</option>
                                        <option value="11">11</option> 
                                        <option value="12">12</option>
                                        <option value="13">13</option>
                                        <option value="14">14</option>
                                        <option value="15">15</option>
                                        <option value="16">16</option>
                                        <option value="17">17</option>
                                        <option value="18">18</option>
                                        <option value="19">19</option>
                                        <option value="20">20</option>
                                        <option value="21">21</option>
                                        <option value="22">22</option>
                                        <option value="23">23</option>
                                        <option value="24">24</option>
                                        <option value="25">25</option>
                                        <option value="26">26</option>
                                        <option value="27">27</option>
                                        <option value="28">28</option>
                                        <option value="29">29</option>
                                        <option value="30">30</option>
                                        <option value="31">31</option>                  
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group has-success">
                            {!! Form::label('name', $lang['name']) !!}
                            {!! Form::text('name', '', array('class' => 'form-control','id' => 'name')) !!}
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group has-warning">
                            {!! Form::label('machine_type', $lang['machine_type']) !!}
                            <select class="form-control" name="machine_type">
                            <option value="null">Nem keresem</option>
                            @foreach ($machine_type as $row)      
                                @if (!empty($row->machine_type))
                                    <option value="{!! $row->machine_type !!}">
                                        {!! $row->machine_type !!}
                                    </option> 
                                @endif
                            @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group has-error">
                            {!! Form::label('type', $lang['type']) !!}
                            {!! Form::text('type', '', array('class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group has-error">
                            {!! Form::label('serial_number', $lang['serial_number']) !!}
                            {!! Form::text('serial_number', '', array('class' => 'form-control')) !!}
                        </div>                    
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group has-warning">
                            {!! Form::label('billing', $lang['billing']) !!}
                            <select class="form-control" name="billing">
                            <option value="null">Nem keresem</option>
                            @foreach ($billing as $row)      
                                @if (!empty($row->billing))
                                    <option value="{!! $row->billing !!}">
                                        {!! $row->billing !!}
                                    </option> 
                                @endif
                            @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group has-error">
                            {!! Form::label('total', $lang['total']) !!}
                            {!! Form::text('total', '', array('class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group has-error">
                            {!! Form::label('repair', $lang['repair']) !!}
                            {!! Form::text('repair', '', array('class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group has-error">
                            {!! Form::label('earnings', $lang['earnings']) !!}
                            {!! Form::text('earnings', '', array('class' => 'form-control')) !!}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group has-error">
                            {!! Form::label('expense', $lang['expense']) !!}
                            {!! Form::text('expense', '', array('class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group has-error">
                            {!! Form::label('iron_core_lenght', $lang['iron_core_lenght']) !!}
                            {!! Form::text('iron_core_lenght', '', array('class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group has-error">
                            {!! Form::label('iron_core_diameter', $lang['iron_core_diameter']) !!}
                            {!! Form::text('iron_core_diameter', '', array('class' => 'form-control')) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group has-error">
                            {!! Form::label('swivel', $lang['swivel']) !!}
                            {!! Form::text('swivel', '', array('class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group has-warning">
                            {!! Form::label('kw', $lang['kw']) !!}
                            <select class="form-control" name="kw">
                            <option value="null">Nem keresem</option>
                            @foreach ($kw as $row)      
                                @if (!empty($row->kw))
                                    <option value="{!! $row->kw !!}">
                                        {!! $row->kw !!}
                                    </option> 
                                @endif
                            @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group has-warning">
                            {!! Form::label('pole', $lang['pole']) !!}
                            <select class="form-control" name="pole">
                            <option value="null">Nem keresem</option>
                            @foreach ($pole as $row)      
                                @if (!empty($row->pole))
                                    <option value="{!! $row->pole !!}">
                                        {!! $row->pole !!}
                                    </option> 
                                @endif
                            @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group has-warning">
                            {!! Form::label('volt', $lang['volt']) !!}
                            <select class="form-control" name="volt">
                            <option value="null">Nem keresem</option>
                            @foreach ($volt as $row)      
                                @if (!empty($row->volt))
                                    <option value="{!! $row->volt !!}">
                                        {!! $row->volt !!}
                                    </option> 
                                @endif
                            @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group has-warning">
                            {!! Form::label('amper', $lang['amper']) !!}
                            <select class="form-control" name="amper">
                            <option value="null">Nem keresem</option>
                            @foreach ($amper as $row)      
                                @if (!empty($row->amper))
                                    <option value="{!! $row->amper !!}">
                                        {!! $row->amper !!}
                                    </option> 
                                @endif
                            @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group has-warning">
                            {!! Form::label('stria', $lang['stria']) !!}
                            <select class="form-control" name="stria">
                            <option value="null">Nem keresem</option>
                            @foreach ($stria as $row)      
                                @if (!empty($row->stria))
                                    <option value="{!! $row->stria !!}">
                                        {!! $row->stria !!}
                                    </option> 
                                @endif
                            @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group has-warning">
                            {!! Form::label('y_or_d', $lang['y_or_d']) !!}
                            <select class="form-control" name="y_or_d">
                            <option value="null">Nem keresem</option>
                            @foreach ($y_or_d as $row)      
                                @if (!empty($row->y_or_d))
                                    <option value="{!! $row->y_or_d !!}">
                                        {!! $row->y_or_d !!}
                                    </option> 
                                @endif
                            @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group has-warning">
                            {!! Form::label('wire', $lang['wire']) !!}
                            <select class="form-control" name="wire">
                            <option value="null">Nem keresem</option>
                            @foreach ($wire as $row)      
                                @if (!empty($row->wire))
                                    <option value="{!! $row->wire !!}">
                                        {!! $row->wire !!}
                                    </option> 
                                @endif
                            @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group has-warning">
                            {!! Form::label('pattern', $lang['pattern']) !!}
                            <select class="form-control" name="pattern">
                            <option value="null">Nem keresem</option>
                            @foreach ($pattern as $row)      
                                @if (!empty($row->pattern))
                                    <option value="{!! $row->pattern !!}">
                                        {!! $row->pattern !!}
                                    </option> 
                                @endif
                            @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group has-error">
                            {!! Form::label('kg', $lang['kg']) !!}
                            {!! Form::text('kg', '', array('class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group has-error">
                            {!! Form::label('ordinal_number', $lang['ordinal_number']) !!}
                            {!! Form::text('ordinal_number', '', array('class' => 'form-control')) !!}
                        </div>
                    </div>
                </div>
                {!! Form::submit('Keresés', array('class' => 'btn btn-default')) !!}
            {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@stop