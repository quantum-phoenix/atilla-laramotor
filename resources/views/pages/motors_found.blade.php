@extends('layout')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <a class="btn btn-default" href="/search">Vissza</a>
    </div>
</div>
<div class="row">
    <div class="col-lg-1">
        <div class="white-box table-responsive table-condensed">
            <table class="table">
                <thead>
                    <tr>
                        <th>{{ $lang['date'] }}</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($motors as $motor)
                    <tr>
                        <td>{!! date('Y m d', strtotime($motor->date)) !!}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-lg-11">
        <div class="white-box table-responsive table-condensed table-hover table-bordered">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>{{ $lang['id'] }}</th>
                        <th>{{ $lang['ordinal_number'] }}</th>                        
                        <th>{{ $lang['name'] }}</th>
                        <th>{{ $lang['machine_type'] }}</th>
                        <th>{{ $lang['type'] }}</th>
                        <th>{{ $lang['serial_number'] }}</th>
                        <th>{{ $lang['billing'] }}</th>
                        <th>{{ $lang['total'] }}</th>
                        <th>{{ $lang['repair'] }}</th>
                        <th>{{ $lang['earnings'] }}</th>
                        <th>{{ $lang['expense'] }}</th>
                        <th>{{ $lang['iron_core_lenght'] }}</th>      
                        <th>{{ $lang['iron_core_diameter'] }}</th>         
                        <th>{{ $lang['swivel'] }}</th>
                        <th>{{ $lang['kw'] }}</th>
                        <th>{{ $lang['pole'] }}</th>
                        <th>{{ $lang['volt'] }}</th>
                        <th>{{ $lang['amper'] }}</th>
                        <th>{{ $lang['stria'] }}</th>
                        <th>{{ $lang['relay'] }}</th>
                        <th>{{ $lang['condenser'] }}</th>
                        <th>{{ $lang['anchor'] }}</th>
                        <th>{{ $lang['stator'] }}</th>
                        <th>{{ $lang['brake'] }}</th>
                        <th>{{ $lang['is_there_serial_number'] }}</th>
                        <th>{{ $lang['y_or_d'] }}</th>
                        <th>{{ $lang['wire'] }}</th>
                        <th>{{ $lang['turning'] }}</th>
                        <th>{{ $lang['component'] }}</th>
                        <th>{{ $lang['envelope'] }}</th>
                        <th>{{ $lang['pattern'] }}</th>
                        <th>{{ $lang['pillow'] }}</th>
                        <th>{{ $lang['seal'] }}</th>
                        <th>{{ $lang['slider_ring'] }}</th>
                        <th>{{ $lang['coal_brush'] }}</th>
                        <th>{{ $lang['is_there_draw'] }}</th>
                        <th>{{ $lang['kg'] }}</th>
                        <th>{{ $lang['primary_phase_resistor'] }}</th>
                        <th>{{ $lang['secondary_phase_resistor'] }}</th>
                        <th>{{ $lang['empty_power'] }}</th>
                        <th>{{ $lang['standard_time'] }}</th>
                        <th>{{ $lang['year_comment'] }}</th>
                        <th>{{ $lang['image'] }}</th>
                        <th>{{ $lang['view'] }}</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($motors as $motor)
                    <tr>
                        <td>{{ $motor->id }}</td>
                        <td>{{ $motor->ordinal_number }}</td>                        
                        <td>                            
                            @if (!empty($motor->person))                                
                                {!! $motor->person->name !!}                               
                            @endif
                            @if(!empty($motor->name_comment))
                                <p class="has_tooltip"></p>
                                <div class="tooltip_content">
                                    <p>{!! $motor->name_comment !!}</p>
                                </div>
                            @endif
                        </td>
                        <td>{{ $motor->machine_type }}</td>
                        <td>
                            {{ $motor->type }}
                            @if(!empty($motor->type_comment))
                                <p class="has_tooltip"></p>
                                <div class="tooltip_content">
                                    <p>{!! $motor->type_comment !!}</p>
                                </div>
                            @endif
                        </td>
                        <td>
                            {{ $motor->serial_number }}
                            @if(!empty($motor->serial_comment))
                                <p class="has_tooltip"></p>
                                <div class="tooltip_content">
                                    <p>{!! $motor->serial_comment !!}</p>
                                </div>
                            @endif
                        </td>
                        <td>{{ $motor->billing }}</td>
                        <td>
                            {{ $motor->total }}
                            @if(!empty($motor->total_comment))
                                <p class="has_tooltip"></p>
                                <div class="tooltip_content">
                                    <p>{!! $motor->total_comment !!}</p>
                                </div>
                            @endif
                        </td>
                        <td>
                            {{ $motor->repair }}
                            @if(!empty($motor->repair_comment))
                                <p class="has_tooltip"></p>
                                <div class="tooltip_content">
                                    <p>{!! nl2br(e($motor->repair_comment)) !!}</p>
                                </div>
                            @endif 
                        </td>
                        <td>
                            {{ $motor->earnings }}
                            @if(!empty($motor->earnings_comment))
                                <p class="has_tooltip"></p>
                                <div class="tooltip_content">
                                    <p>{!! $motor->earnings_comment !!}</p>
                                </div>
                            @endif 
                        </td>
                        <td>
                            {{ $motor->expense }}
                            @if(!empty($motor->expense_comment))
                                <p class="has_tooltip"></p>
                                <div class="tooltip_content">
                                    <p>{!! $motor->expense_comment !!}</p>
                                </div>
                            @endif 
                        </td>
                        <td>
                            {{ $motor->iron_core_lenght }}
                            @if(!empty($motor->iron_core_lenght_comment))
                                <p class="has_tooltip"></p>
                                <div class="tooltip_content">
                                    <p>{!! $motor->iron_core_lenght_comment !!}</p>
                                </div>
                            @endif 
                        </td>   
                        <td>
                            {{ $motor->iron_core_diameter }}
                            @if(!empty($motor->iron_core_diameter_comment))
                                <p class="has_tooltip"></p>
                                <div class="tooltip_content">
                                    <p>{!! $motor->iron_core_diameter_comment !!}</p>
                                </div>
                            @endif 
                        </td>    
                        <td>{{ $motor->swivel }}</td>
                        <td>
                            {{ $motor->kw }}
                            @if(!empty($motor->kw_comment))
                                <p class="has_tooltip"></p>
                                <div class="tooltip_content">
                                    <p>{!! $motor->kw_comment !!}</p>
                                </div>
                            @endif 
                        </td>
                        <td>
                            {{ $motor->pole }}
                            @if(!empty($motor->pole_comment))
                                <p class="has_tooltip"></p>
                                <div class="tooltip_content">
                                    <p>{!! $motor->pole_comment !!}</p>
                                </div>
                            @endif 
                        </td>
                        <td>
                            {{ $motor->volt }}
                            @if(!empty($motor->volt_comment))
                                <p class="has_tooltip"></p>
                                <div class="tooltip_content">
                                    <p>{!! $motor->volt_comment !!}</p>
                                </div>
                            @endif 
                        </td>
                        <td>
                            {{ $motor->amper }}
                            @if(!empty($motor->amper_comment))
                                <p class="has_tooltip"></p>
                                <div class="tooltip_content">
                                    <p>{!! $motor->amper_comment !!}</p>
                                </div>
                            @endif 
                        </td>
                        <td>{{ $motor->stria }}</td>
                        <td>
                            @if($motor->relay == 1)
                                Van
                            @elseif($motor->relay == 0)
                                Nincs
                            @endif
                            @if(!empty($motor->relay_comment))
                                <p class="has_tooltip"></p>
                                <div class="tooltip_content">
                                    <p>{!! $motor->relay_comment !!}</p>
                                </div>
                            @endif                        
                        </td>
                        <td>
                            @if($motor->condenser == 1)
                                Van
                            @elseif($motor->condenser == 0)
                                Nincs
                            @endif 
                            @if(!empty($motor->condenser_comment))
                                <p class="has_tooltip"></p>
                                <div class="tooltip_content">
                                    <p>{!! $motor->condenser_comment !!}</p>
                                </div>
                            @endif                       
                        </td>  
                        <td>
                            @if($motor->anchor == 1)
                                Van
                            @elseif($motor->anchor == 0)
                                Nincs
                            @endif 
                            @if(!empty($motor->anchor_comment))
                                <p class="has_tooltip"></p>
                                <div class="tooltip_content">
                                    <p>{!! $motor->anchor_comment !!}</p>
                                </div>
                            @endif                        
                        </td>
                               <td>
                            @if($motor->stator == 1)
                                Van
                            @elseif($motor->stator == 0)
                                Nincs
                            @endif  
                            @if(!empty($motor->stator_comment))
                                <p class="has_tooltip"></p>
                                <div class="tooltip_content">
                                    <p>{!! $motor->stator_comment !!}</p>
                                </div>
                            @endif                       
                        </td>
                                 <td>
                            @if($motor->brake == 1)
                                Van
                            @elseif($motor->brake == 0)
                                Nincs
                            @endif                        
                        </td>
                        <td>
                            @if($motor->is_there_serial_number == 1)
                                Van
                            @elseif($motor->is_there_serial_number == 0)
                                Nincs
                            @endif
                            @if(!empty($motor->is_there_serial_number_comment))
                                <p class="has_tooltip"></p>
                                <div class="tooltip_content">
                                    <p>{!! $motor->is_there_serial_number_comment !!}</p>
                                </div>
                            @endif                         
                        </td>
                        <td>
                            {{ $motor->y_or_d }}
                            @if(!empty($motor->y_or_d_comment))
                                <p class="has_tooltip"></p>
                                <div class="tooltip_content">
                                    <p>{!! $motor->y_or_d_comment !!}</p>
                                </div>
                            @endif
                        </td>
                        <td>
                            {{ $motor->wire }}
                            @if(!empty($motor->wire_comment))
                                <p class="has_tooltip"></p>
                                <div class="tooltip_content">
                                    <p>{!! $motor->wire_comment !!}</p>
                                </div>
                            @endif
                        </td>
                        <td>
                            @if($motor->turning == 1)
                                Van
                            @elseif($motor->turning == 0)
                                Nincs
                            @endif
                            @if(!empty($motor->turning_comment))
                                <p class="has_tooltip"></p>
                                <div class="tooltip_content">
                                    <p>{!! $motor->turning_comment !!}</p>
                                </div>
                            @endif                       
                        </td>
                        <td>
                            @if($motor->component == 1)
                                Van
                            @elseif($motor->component == 0)
                                Nincs
                            @endif
                            @if(!empty($motor->component_comment))
                                <p class="has_tooltip"></p>
                                <div class="tooltip_content">
                                    <p>{!! $motor->component_comment !!}</p>
                                </div>
                            @endif                       
                        </td>
                         <td>
                            @if($motor->envelope == 1)
                                Van
                            @elseif($motor->envelope == 0)
                                Nincs
                            @endif                        
                        </td>
                        <td>
                            {{ $motor->pattern }}
                            @if(!empty($motor->pattern_comment))
                                <p class="has_tooltip"></p>
                                <div class="tooltip_content">
                                    <p>{!! $motor->pattern_comment !!}</p>
                                </div>
                            @endif
                        </td>
                        <td>
                            @if($motor->pillow == 1)
                                Van
                            @elseif($motor->pillow == 0)
                                Nincs
                            @endif
                            @if(!empty($motor->pillow_comment))
                                <p class="has_tooltip"></p>
                                <div class="tooltip_content">
                                    <p>{!! $motor->pillow_comment !!}</p>
                                </div>
                            @endif                      
                        </td>
                        <td>
                            @if($motor->seal == 1)
                                Van
                            @elseif($motor->seal == 0)
                                Nincs
                            @endif
                            @if(!empty($motor->seal_comment))
                                <p class="has_tooltip"></p>
                                <div class="tooltip_content">
                                    <p>{!! $motor->seal_comment !!}</p>
                                </div>
                            @endif                      
                        </td>
                        <td>
                            @if($motor->slider_ring == 1)
                                Van
                            @elseif($motor->slider_ring == 0)
                                Nincs
                            @endif  
                            @if(!empty($motor->slider_ring_comment))
                                <p class="has_tooltip"></p>
                                <div class="tooltip_content">
                                    <p>{!! $motor->slider_ring_comment !!}</p>
                                </div>
                            @endif                      
                        </td>
                        <td>
                            @if($motor->coal_brush == 1)
                                Van
                            @elseif($motor->coal_brush == 0)
                                Nincs
                            @endif
                            @if(!empty($motor->coal_brush_comment))
                                <p class="has_tooltip"></p>
                                <div class="tooltip_content">
                                    <p>{!! $motor->coal_brush_comment !!}</p>
                                </div>
                            @endif                      
                        </td>  
                        <td>
                            @if($motor->is_there_draw == 1)
                                Van
                            @elseif($motor->is_there_draw == 0)
                                Nincs
                            @endif
                            @if(!empty($motor->is_there_draw_comment))
                                <p class="has_tooltip"></p>
                                <div class="tooltip_content">
                                    <p>{!! $motor->is_there_draw_comment !!}</p>
                                </div>
                            @endif                        
                        </td>
                        <td>
                            {{ $motor->kg }}
                            @if(!empty($motor->kg_comment))
                                <p class="has_tooltip"></p>
                                <div class="tooltip_content">
                                    <p>{!! $motor->kg_comment !!}</p>
                                </div>
                            @endif
                        </td>
                        <td>
                            @if($motor->primary_phase_resistor == 1)
                                Van
                            @elseif($motor->primary_phase_resistor == 0)
                                Nincs
                            @endif 
                            @if(!empty($motor->primary_phase_resistor_comment))
                                <p class="has_tooltip"></p>
                                <div class="tooltip_content">
                                    <p>{!! $motor->primary_phase_resistor_comment !!}</p>
                                </div>
                            @endif
                        </td>
                        <td>
                            @if($motor->secondary_phase_resistor == 1)
                                Van
                            @elseif($motor->secondary_phase_resistor == 0)
                                Nincs
                            @endif 
                            @if(!empty($motor->secondary_phase_resistor_comment))
                                <p class="has_tooltip"></p>
                                <div class="tooltip_content">
                                    <p>{!! $motor->secondary_phase_resistor_comment !!}</p>
                                </div>
                            @endif
                        </td>
                        <td>
                            @if($motor->empty_power == 1)
                                Van
                            @elseif($motor->empty_power == 0)
                                Nincs
                            @endif
                            @if(!empty($motor->empty_power_comment))
                                <p class="has_tooltip"></p>
                                <div class="tooltip_content">
                                    <p>{!! $motor->empty_power_comment !!}</p>
                                </div>
                            @endif
                        </td> 
                        <td>
                            @if($motor->standard_time == 1)
                                Van
                            @elseif($motor->standard_time == 0)
                                Nincs
                            @endif
                            @if(!empty($motor->standard_time_comment))
                                <p class="has_tooltip"></p>
                                <div class="tooltip_content">
                                    <p>{!! $motor->standard_time_comment !!}</p>
                                </div>
                            @endif
                        </td>
                        <td>{{ $motor->year_comment }}</td>
                        <td>
                            @if (!empty($motor->picture_id))
                                <ul class="list-inline">
                                <?php $i = 1 ?>
                                @foreach($motor->picture as $row)                                    
                                    <li><a class="btn btn-link btn-xs fancybox" rel="{{ $row->picture_id }}" href="/uploads/{{ $row->picture_file_name }}" title="Kép">Kép {{ $i }}</a></li>
                                        <?php $i++ ?> 
                                @endforeach
                                </ul>
                            @else
                                Nincs kép
                            @endif
                        </td>
                        <td><a class="btn btn-link" href="/motors/{{ $motor->id }}">Megnéz</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="found-box white-box">
                @if (!empty($count))  
                    <p>{{ $count }} sort találtam.</p>
                @endif  
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        {!! $motors->appends(Input::all())->render() !!}
    </div>
</div>  
@stop