@extends('layout')

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<a class="btn btn-default" href="/database">Vissza</a>
			<a class="btn btn-default" href="/motors/{{ $motors->id }}/edit">Szerkesztés</a>
			{!! Form::open(array('url' => 'motors/' . $motors->id, 'class' => 'pull-right')) !!}
            	{!! Form::hidden('_method', 'DELETE') !!}
            	{!! Form::submit('Törlés', array('class' => 'btn btn-default')) !!}
			{!! Form::close() !!}
		</div>
	</div>
	<table class="table table-hover white-box">
	    <thead>
	        <tr>
	            <th>Név</th>
	            <th>Adat</th>
	        </tr>
	    </thead>
	    <tbody>	    
	    	<tr>
	            <td>{{ $lang['image'] }}</td>
	            <td>
	            	@if(!empty($motors->image))
	            		<img class="responsive-image" src="/uploads/{{ $motors->image }}" alt="{{ $motors->image }}">
	            	@else
	            		Nincs kép
	            	@endif
	            </td>
	        </tr>
	        <tr>
	            <td>{{ $lang['year'] }}</td>
	            <td>{{ $motors->year }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['year_comment'] }}</td>
	            <td>{{ $motors->year_comment }}</td>
	        </tr>
			<tr>
	            <td>{{ $lang['date'] }}</td>
	            <td>{{ $motors->date }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['name'] }}</td>
	            <td>{{ $motors->name }}</td>
	        </tr>
			<tr>
	            <td>{{ $lang['name_comment'] }}</td>
	            <td>{{ $motors->name_comment }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['machine_type'] }}</td>
	            <td>{{ $motors->machine_type }}</td>
	        </tr>
			<tr>
	            <td>{{ $lang['type'] }}</td>
	            <td>{{ $motors->type }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['type_comment'] }}</td>
	            <td>{{ $motors->type_comment }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['serial_number'] }}</td>
	            <td>{{ $motors->serial_number }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['serial_comment'] }}</td>
	            <td>{{ $motors->serial_comment }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['billing'] }}</td>
	            <td>{{ $motors->billing }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['total'] }}</td>
	            <td>{{ $motors->total }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['total_comment'] }}</td>
	            <td>{{ $motors->total_comment }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['repair'] }}</td>
	            <td>{{ $motors->repair }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['repair_comment'] }}</td>
	            <td><textarea disabled class="form-control" rows="10">{{ $motors->repair_comment }}</textarea></td>
	        </tr>
	        <tr>
	            <td>{{ $lang['earnings'] }}</td>
	            <td>{{ $motors->earnings }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['earnings_comment'] }}</td>
	            <td>{{ $motors->earnings_comment }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['expense'] }}</td>
	            <td>{{ $motors->expense }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['expense_comment'] }}</td>
	            <td>{{ $motors->expense_comment }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['iron_core_lenght'] }}</td>
	            <td>{{ $motors->iron_core_lenght }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['iron_core_lenght_comment'] }}</td>
	            <td>{{ $motors->iron_core_lenght_comment }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['iron_core_diameter'] }}</td>
	            <td>{{ $motors->iron_core_diameter }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['iron_core_diameter_comment'] }}</td>
	            <td>{{ $motors->iron_core_diameter_comment }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['swivel'] }}</td>
	            <td>{{ $motors->swivel }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['kw'] }}</td>
	            <td>{{ $motors->kw }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['kw_comment'] }}</td>
	            <td>{{ $motors->kw_comment }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['pole'] }}</td>
	            <td>{{ $motors->pole }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['pole_comment'] }}</td>
	            <td>{{ $motors->pole_comment }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['volt'] }}</td>
	            <td>{{ $motors->volt }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['volt_comment'] }}</td>
	            <td>{{ $motors->volt_comment }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['amper'] }}</td>
	            <td>{{ $motors->amper }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['amper_comment'] }}</td>
	            <td>{{ $motors->amper_comment }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['stria'] }}</td>
	            <td>{{ $motors->stria }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['relay'] }}</td>
	            <td>{{ $motors->relay }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['relay_comment'] }}</td>
	            <td>{{ $motors->relay_comment }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['condenser'] }}</td>
	            <td>{{ $motors->condenser }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['condenser_comment'] }}</td>
	            <td>{{ $motors->condenser_comment }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['anchor'] }}</td>
	            <td>{{ $motors->anchor }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['anchor_comment'] }}</td>
	            <td>{{ $motors->anchor_comment }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['stator'] }}</td>
	            <td>{{ $motors->stator }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['stator_comment'] }}</td>
	            <td>{{ $motors->stator_comment }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['brake'] }}</td>
	            <td>{{ $motors->brake }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['is_there_serial_number'] }}</td>
	            <td>{{ $motors->is_there_serial_number }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['is_there_serial_number_comment'] }}</td>
	            <td>{{ $motors->is_there_serial_number_comment }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['y_or_d'] }}</td>
	            <td>{{ $motors->y_or_d }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['y_or_d_comment'] }}</td>
	            <td>{{ $motors->y_or_d_comment }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['wire'] }}</td>
	            <td>{{ $motors->wire }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['wire_comment'] }}</td>
	            <td>{{ $motors->wire_comment }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['turning'] }}</td>
	            <td>{{ $motors->turning }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['turning_comment'] }}</td>
	            <td>{{ $motors->turning_comment }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['component'] }}</td>
	            <td>{{ $motors->component }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['component_comment'] }}</td>
	            <td>{{ $motors->component_comment }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['envelope'] }}</td>
	            <td>{{ $motors->envelope }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['pattern'] }}</td>
	            <td>{{ $motors->pattern }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['pattern_comment'] }}</td>
	            <td>{{ $motors->pattern_comment }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['pillow'] }}</td>
	            <td>{{ $motors->pillow }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['pillow_comment'] }}</td>
	            <td>{{ $motors->pillow_comment }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['seal'] }}</td>
	            <td>{{ $motors->seal }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['seal_comment'] }}</td>
	            <td>{{ $motors->seal_comment }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['slider_ring'] }}</td>
	            <td>{{ $motors->slider_ring }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['slider_ring_comment'] }}</td>
	            <td>{{ $motors->slider_ring_comment }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['coal_brush'] }}</td>
	            <td>{{ $motors->coal_brush }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['coal_brush_comment'] }}</td>
	            <td>{{ $motors->coal_brush_comment }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['is_there_draw'] }}</td>
	            <td>{{ $motors->is_there_draw }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['is_there_draw_comment'] }}</td>
	            <td>{{ $motors->is_there_draw_comment }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['kg'] }}</td>
	            <td>{{ $motors->kg }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['kg_comment'] }}</td>
	            <td>{{ $motors->kg_comment }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['primary_phase_resistor'] }}</td>
	            <td>{{ $motors->primary_phase_resistor }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['primary_phase_resistor_comment'] }}</td>
	            <td>{{ $motors->primary_phase_resistor_comment }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['secondary_phase_resistor'] }}</td>
	            <td>{{ $motors->secondary_phase_resistor }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['secondary_phase_resistor_comment'] }}</td>
	            <td>{{ $motors->secondary_phase_resistor_comment }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['empty_power'] }}</td>
	            <td>{{ $motors->empty_power }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['empty_power_comment'] }}</td>
	            <td>{{ $motors->empty_power_comment }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['standard_time'] }}</td>
	            <td>{{ $motors->standard_time }}</td>
	        </tr>
	        <tr>
	            <td>{{ $lang['standard_time_comment'] }}</td>
	            <td>{{ $motors->standard_time_comment }}</td>
	        </tr>	        
		</tbody>
	</table>
@stop