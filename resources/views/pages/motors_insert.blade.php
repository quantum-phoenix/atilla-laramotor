@extends('layout')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <a class="btn btn-default" href="/">Vissza</a>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
            <h1>Új bejegyzés</h1>
            {!! Form::open(array('method' =>'POST','url' => 'motors','class' => '','files'=> true)) !!}
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            {!! Form::label('images[]', $lang['image']) !!}
                            {!! Form::file('images[]', array('multiple'=>true)) !!}
                        </div>                      
                    </div>
                    <div class="col-lg-6">
                        @if ($errors->has('image'))
                            <div class="alert alert-danger" role="alert"> 
                                <p>{{ $errors->first('image') }}</p>
                            </div>
                        @endif
                        @if(Session::has('image-error'))
                            <div class="alert alert-danger" role="alert">
                                <p class="errors">{!! Session::get('error') !!}</p>
                            </div>
                        @endif 
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-2">
                        <div class="form-group">
                            {!! Form::label('relay', $lang['relay']) !!}
                            <select class="form-control" name="relay">
                                <option value="false">Nincs</option>
                                <option value="true">Van</option>                       
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            {!! Form::label('condenser', $lang['condenser']) !!}
                            <select class="form-control" name="condenser">
                                <option value="false">Nincs</option>
                                <option value="true">Van</option>                       
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            {!! Form::label('anchor', $lang['anchor']) !!}
                            <select class="form-control" name="anchor">
                                 <option value="false">Nincs</option>
                                <option value="true">Van</option>                       
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            {!! Form::label('stator', $lang['stator']) !!}
                            <select class="form-control" name="stator">
                                <option value="false">Nincs</option>
                                <option value="true">Van</option>                       
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            {!! Form::label('brake', $lang['brake']) !!}
                            <select class="form-control" name="brake">
                                <option value="false">Nincs</option>
                                <option value="true">Van</option>                       
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            {!! Form::label('is_there_serial_number', $lang['is_there_serial_number']) !!}
                            <select class="form-control" name="is_there_serial_number">
                                <option value="false">Nincs</option>
                                <option value="true">Van</option>                       
                            </select>
                        </div>
                    </div>
                </div>    

                <div class="row">
                    <div class="col-lg-2">
                        <div class="form-group">
                            {!! Form::label('turning', $lang['turning']) !!}
                            <select class="form-control" name="turning">
                                <option value="false">Nincs</option>
                                <option value="true">Van</option>                       
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            {!! Form::label('component', $lang['component']) !!}
                            <select class="form-control" name="component">                                
                                <option value="false">Nincs</option>
                                <option value="true">Van</option>                       
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            {!! Form::label('envelope', $lang['envelope']) !!}
                            <select class="form-control" name="envelope">
                                <option value="false">Nincs</option>
                                <option value="true">Van</option>                       
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            {!! Form::label('pillow', $lang['pillow']) !!}
                            <select class="form-control" name="pillow">
                                <option value="false">Nincs</option>
                                <option value="true">Van</option>                       
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            {!! Form::label('seal', $lang['seal']) !!}
                            <select class="form-control" name="seal">
                                <option value="false">Nincs</option>
                                <option value="true">Van</option>                       
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            {!! Form::label('slider_ring', $lang['slider_ring']) !!}
                            <select class="form-control" name="slider_ring">
                                <option value="false">Nincs</option>
                                <option value="true">Van</option>                       
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-2">
                        <div class="form-group">
                            {!! Form::label('coal_brush', $lang['coal_brush']) !!}
                            <select class="form-control" name="coal_brush">
                                <option value="false">Nincs</option>
                                <option value="true">Van</option>                       
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            {!! Form::label('is_there_draw', $lang['is_there_draw']) !!}
                            <select class="form-control" name="is_there_draw">
                                <option value="false">Nincs</option>
                                <option value="true">Van</option>                       
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            {!! Form::label('primary_phase_resistor', $lang['primary_phase_resistor']) !!}
                            <select class="form-control" name="primary_phase_resistor">
                                <option value="false">Nincs</option>
                                <option value="true">Van</option>                       
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            {!! Form::label('secondary_phase_resistor', $lang['secondary_phase_resistor']) !!}
                            <select class="form-control" name="secondary_phase_resistor">
                                <option value="false">Nincs</option>
                                <option value="true">Van</option>                       
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            {!! Form::label('empty_power', $lang['empty_power']) !!}
                            <select class="form-control" name="empty_power">
                                <option value="false">Nincs</option>
                                <option value="true">Van</option>                       
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            {!! Form::label('standard_time', $lang['standard_time']) !!}
                            <select class="form-control" name="standard_time">
                                <option value="false">Nincs</option>
                                <option value="true">Van</option>                       
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            {!! Form::label('date', $lang['date']) !!}
                            <div class="row">
                                <div class="col-lg-4">
                                    <select class="form-control" name="selected_year" readonly>
                                        <option value="2005">2005</option>
                                        <option value="2006">2006</option>
                                        <option value="2007">2007</option> 
                                        <option value="2008">2008</option> 
                                        <option value="2009">2009</option> 
                                        <option value="2010">2010</option> 
                                        <option value="2011">2011</option>
                                        <option value="2012">2012</option> 
                                        <option value="2013">2013</option> 
                                        <option value="2014">2014</option>
                                        <option value="2015">2015</option>
                                        <option value="2016">2016</option>                    
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <select class="form-control" name="selected_month" readonly>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option> 
                                        <option value="4">4</option> 
                                        <option value="5">5</option> 
                                        <option value="6">6</option> 
                                        <option value="7">7</option> 
                                        <option value="8">8</option> 
                                        <option value="9">9</option> 
                                        <option value="10">10</option>
                                        <option value="11">11</option> 
                                        <option value="12">12</option>                      
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <select class="form-control" name="selected_day" readonly>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option> 
                                        <option value="4">4</option> 
                                        <option value="5">5</option> 
                                        <option value="6">6</option> 
                                        <option value="7">7</option> 
                                        <option value="8">8</option> 
                                        <option value="9">9</option> 
                                        <option value="10">10</option>
                                        <option value="11">11</option> 
                                        <option value="12">12</option>
                                        <option value="13">13</option>
                                        <option value="14">14</option>
                                        <option value="15">15</option>
                                        <option value="16">16</option>
                                        <option value="17">17</option>
                                        <option value="18">18</option>
                                        <option value="19">19</option>
                                        <option value="20">20</option>
                                        <option value="21">21</option>
                                        <option value="22">22</option>
                                        <option value="23">23</option>
                                        <option value="24">24</option>
                                        <option value="25">25</option>
                                        <option value="26">26</option>
                                        <option value="27">27</option>
                                        <option value="28">28</option>
                                        <option value="29">29</option>
                                        <option value="30">30</option>
                                        <option value="31">31</option>                  
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group has-success">
                            {!! Form::label('name', $lang['name']) !!}
                            {!! Form::text('name', '', array('class' => 'form-control','id' => 'name')) !!}
                            @if ($errors->has('name'))
                            <div class="alert alert-danger" role="alert"> 
                                <p>{{ $errors->first('name') }}</p>
                            </div>
                        @endif
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group has-warning">
                            {!! Form::label('machine_type', $lang['machine_type']) !!}
                            <select class="form-control" name="machine_type">
                            <option value="null">Nem keresem</option>
                            @foreach ($machine_type as $row)      
                                @if (!empty($row->machine_type))
                                    <option value="{!! $row->machine_type !!}">
                                        {!! $row->machine_type !!}
                                    </option> 
                                @endif
                            @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group has-error">
                            {!! Form::label('type', $lang['type']) !!}
                            {!! Form::text('type', '', array('class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group has-error">
                            {!! Form::label('serial_number', $lang['serial_number']) !!}
                            {!! Form::text('serial_number', '', array('class' => 'form-control')) !!}
                        </div>                    
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group has-warning">
                            {!! Form::label('billing', $lang['billing']) !!}
                            <select class="form-control" name="billing">
                            <option value="null">Nem keresem</option>
                            @foreach ($billing as $row)      
                                @if (!empty($row->billing))
                                    <option value="{!! $row->billing !!}">
                                        {!! $row->billing !!}
                                    </option> 
                                @endif
                            @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group has-error">
                            {!! Form::label('total', $lang['total']) !!}
                            {!! Form::text('total', '', array('class' => 'form-control')) !!}
                        </div>
                        @if ($errors->has('total'))
                            <div class="alert alert-danger" role="alert"> 
                                <p>{{ $errors->first('total') }}</p>
                            </div>
                        @endif
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group has-error">
                            {!! Form::label('repair', $lang['repair']) !!}
                            {!! Form::text('repair', '', array('class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group has-error">
                            {!! Form::label('earnings', $lang['earnings']) !!}
                            {!! Form::text('earnings', '', array('class' => 'form-control')) !!}
                        </div>
                        @if ($errors->has('earnings'))
                            <div class="alert alert-danger" role="alert"> 
                                <p>{{ $errors->first('earnings') }}</p>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group has-error">
                            {!! Form::label('expense', $lang['expense']) !!}
                            {!! Form::text('expense', '', array('class' => 'form-control')) !!}
                        </div>
                        @if ($errors->has('expense'))
                            <div class="alert alert-danger" role="alert"> 
                                <p>{{ $errors->first('expense') }}</p>
                            </div>
                        @endif

                    </div>
                    <div class="col-lg-4">
                        <div class="form-group has-error">
                            {!! Form::label('iron_core_lenght', $lang['iron_core_lenght']) !!}
                            {!! Form::text('iron_core_lenght', '', array('class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group has-error">
                            {!! Form::label('iron_core_diameter', $lang['iron_core_diameter']) !!}
                            {!! Form::text('iron_core_diameter', '', array('class' => 'form-control')) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group has-error">
                            {!! Form::label('swivel', $lang['swivel']) !!}
                            {!! Form::text('swivel', '', array('class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group has-error">
                            {!! Form::label('kw', $lang['kw']) !!}
                            {!! Form::text('kw', '', array('class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group has-error">
                            {!! Form::label('pole', $lang['pole']) !!}
                            {!! Form::text('pole', '', array('class' => 'form-control')) !!}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group has-error">
                            {!! Form::label('volt', $lang['volt']) !!}
                            {!! Form::text('volt', '', array('class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group has-error">
                            {!! Form::label('amper', $lang['amper']) !!}
                            {!! Form::text('amper', '', array('class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group has-error">
                            {!! Form::label('stria', $lang['stria']) !!}
                            {!! Form::text('stria', '', array('class' => 'form-control')) !!}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group has-error">
                            {!! Form::label('y_or_d', $lang['y_or_d']) !!}
                            {!! Form::text('y_or_d', '', array('class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group has-error">
                            {!! Form::label('wire', $lang['wire']) !!}
                            {!! Form::text('wire', '', array('class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group has-error">
                            {!! Form::label('pattern', $lang['pattern']) !!}
                            {!! Form::text('pattern', '', array('class' => 'form-control')) !!}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group has-error">
                            {!! Form::label('kg', $lang['kg']) !!}
                            {!! Form::text('kg', '', array('class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group has-error">
                            {!! Form::label('ordinal_number', $lang['ordinal_number']) !!}
                            {!! Form::text('ordinal_number', '', array('class' => 'form-control')) !!}
                        </div>
                    </div>
                </div>
                {!! Form::submit('Új bejegyzés', array('class' => 'btn btn-default')) !!}
                @if(Session::has('success'))
                    <div class="alert alert-success" role="alert">
                        <p>{!! Session::get('success') !!}</p>
                    </div>
                @endif
            {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@stop