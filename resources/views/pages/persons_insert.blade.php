@extends('layout')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <a class="btn btn-default" href="/persons">Vissza</a>
    </div>
</div>

<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
            <div class="panel-body">
            <h1>{{ trans('messages.new_person') }}</h1>

            {!! Form::open(array('method' =>'POST','url' => 'persons','class' => '')) !!}
            <div class="form-group has-error">
                {!! Form::label('name', trans('messages.name')) !!}
                {!! Form::text('name', '', array('class' => 'form-control')) !!}
            </div>
            @if ($errors->has('name'))
                <div class="alert alert-danger" role="alert"> 
                    <p>{{ $errors->first('name') }}</p>
                </div>
            @endif

            <div class="form-group has-error">
                {!! Form::label('wire_phone_number', trans('messages.wire_phone_number')) !!}
                {!! Form::text('wire_phone_number', '', array('class' => 'form-control')) !!}
            </div>
            @if ($errors->has('wire_phone_number'))
                <div class="alert alert-danger" role="alert"> 
                    <p>{{ $errors->first('wire_phone_number') }}</p>
                </div>
            @endif

            <div class="form-group has-error">
                {!! Form::label('mobile_phone_number', trans('messages.mobile_phone_number')) !!}
                {!! Form::text('mobile_phone_number', '', array('class' => 'form-control')) !!}
            </div>
            @if ($errors->has('mobile_phone_number'))
                <div class="alert alert-danger" role="alert"> 
                    <p>{{ $errors->first('mobile_phone_number') }}</p>
                </div>
            @endif

            <div class="form-group has-error">
                {!! Form::label('bill_address', trans('messages.bill_address')) !!}
                {!! Form::text('bill_address', '', array('class' => 'form-control')) !!}
            </div>
            @if ($errors->has('bill_address'))
                <div class="alert alert-danger" role="alert"> 
                    <p>{{ $errors->first('bill_address') }}</p>
                </div>
            @endif

            <div class="form-group has-error">
                {!! Form::label('comment', trans('messages.comment')) !!}
                {!! Form::text('comment', '', array('class' => 'form-control')) !!}
            </div>
            @if ($errors->has('comment'))
                <div class="alert alert-danger" role="alert"> 
                    <p>{{ $errors->first('comment') }}</p>
                </div>
            @endif

            {!! Form::submit(trans('messages.new_person'), array('class' => 'btn btn-default')) !!}
                @if(Session::has('success'))
                    <div class="alert alert-success" role="alert">
                        <p>{!! Session::get('success') !!}</p>
                    </div>
                @endif
            {!! Form::close() !!}
            </div>
        </div>
	</div>
</div>

<div class="row">
    <div class="col-lg-12">        
    </div>
</div>

@stop

