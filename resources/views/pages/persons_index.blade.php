@extends('layout')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <a class="btn btn-default" href="/">Vissza</a>
        <a class="btn btn-default" href="/persons/create">{{ trans('messages.new_person') }}</a>
    </div>
</div>

<div class="row">
	<div class="col-lg-12">
		<div class="white-box table-responsive table-condensed table-hover table-bordered">
            <table class="table table-bordered">
                <thead>
                    <tr>
                    	<th>{{ trans('messages.name') }}</th>
                    	<th>{{ trans('messages.wire_phone_number') }}</th>
                    	<th>{{ trans('messages.mobile_phone_number') }}</th>
                    	<th>{{ trans('messages.bill_address') }}</th>
                    	<th>{{ trans('messages.comment') }}</th>
                    	<th>{{ trans('messages.edit') }}</th>
                    </tr>
                <thead>
                <tbody>
                @foreach ($persons as $row)
                	<tr>
                    	<th>{{ $row->name }}</th>
                    	<th>{{ $row->wire_phone_number }}</th>
                    	<th>{{ $row->mobile_phone_number }}</th>
                    	<th>{{ $row->bill_address }}</th>
                    	<th>{{ $row->comment }}</th>
                    	<th>
                        <a class="btn btn-link" href="/persons/{{ $row->id }}/edit">{{ trans('messages.edit') }}</a>
                        </th>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
	</div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="found-box white-box">
                @if (!empty($count))  
                    <p>{{ $count }} sort találtam.</p>
                @endif  
        </div>
    </div>
</div>
@stop

