<?php

return [
    'name' => 'Név',
    'wire_phone_number' => 'Vezetékes telefonszámok',
    'mobile_phone_number' => 'Mobil telefonszámok',
    'bill_address' => 'Számlázási cím',
    'comment' => 'Megjegyzés',
    'edit' => 'Szerkesztés',
    'new_person' => 'Új ember felvétele'
];