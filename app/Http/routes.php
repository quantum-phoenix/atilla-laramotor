<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', 'Home@index');
// Route::get('/motor/show/{id}', 'Home@show');
// Route::get('/motor/edit/{id}', 'Home@edit');
// Route::patch('/motor/update/{id}', 'Home@update');
// Route::get('/motor/delete/{id}', 'Home@destroy');
// Route::post('/motor/store', 'Home@store');
// Route::get('/motor/insert', 'Home@create');

//Route::resource('motors', 'Home');

Route::get('/', function () {
    return Redirect::to('motors'); 
});
Route::get('/search', 'MotorsController@search');
Route::get('/database', 'MotorsController@database');
Route::get('/found', 'MotorsController@found');
Route::resource('motors', 'MotorsController');
Route::resource('persons', 'PersonsController');


Route::get('search/autocomplete/name', 'AutocompleteController@autocomplete_name');
Route::get('/prototype', 'PrototypeController@names');