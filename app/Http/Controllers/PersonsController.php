<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Persons;
use DB;
use Validator;
use Input;
use Redirect;
use Request;
use Session;

class PersonsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $persons = Persons::get();
        $count = Persons::count();
        return view('pages.persons_index', compact('persons','count')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.persons_insert');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(            
            'name'            => 'string',
            'wire_phone_number'            => 'string',
            'mobile_phone_number'            => 'string',
            'bill_address'            => 'string',
            'comment'            => 'string',
        );
        $messages = array(
            'string'  => 'A beírt szőveg nem szőveg. :)'
        );

        $validator = Validator::make(Input::all(), $rules, $messages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::back()->withInput()->withErrors($validator);

        }

        $person = new Persons;

        if (Input::has('name')) {
            $q_string = $request::get('name');
            if(!empty($q_string)){
                $person->name = $q_string;   
            }
                   
        }
        if (Input::has('wire_phone_number')) {
            $q_string = $request::get('wire_phone_number');
            if(!empty($q_string)){
                $person->wire_phone_number = $q_string;   
            }
                   
        }
        if (Input::has('mobile_phone_number')) {
            $q_string = $request::get('mobile_phone_number');
            if(!empty($q_string)){
                $person->mobile_phone_number = $q_string;   
            }
                   
        }
        if (Input::has('bill_address')) {
            $q_string = $request::get('bill_address');
            if(!empty($q_string)){
                $person->bill_address = $q_string;   
            }
                   
        }
        if (Input::has('comment')) {
            $q_string = $request::get('comment');
            if(!empty($q_string)){
                $person->comment = $q_string;   
            }
                   
        }

        $person->save();
        Session::flash('success', 'Az új bejegyzés sikeresen hozzáadva.');
        return Redirect::to('persons/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $person = Persons::find($id);     
        return view('pages.persons_edit', compact('person'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(            
            'name'            => 'string',
            'wire_phone_number'            => 'string',
            'mobile_phone_number'            => 'string',
            'bill_address'            => 'string',
            'comment'            => 'string',
        );
        $messages = array(
            'string'  => 'A beírt szőveg nem szőveg. :)'
        );

        $validator = Validator::make(Input::all(), $rules, $messages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::back()->withInput()->withErrors($validator);

        }
        $person = Persons::find($id);
        if (Input::has('name')) {
            $q_string = $request::get('name');
            if(!empty($q_string)){
                $person->name = $q_string;   
            }
                   
        }
        if (Input::has('wire_phone_number')) {
            $q_string = $request::get('wire_phone_number');
            if(!empty($q_string)){
                $person->wire_phone_number = $q_string;   
            }
                   
        }
        if (Input::has('mobile_phone_number')) {
            $q_string = $request::get('mobile_phone_number');
            if(!empty($q_string)){
                $person->mobile_phone_number = $q_string;   
            }
                   
        }
        if (Input::has('bill_address')) {
            $q_string = $request::get('bill_address');
            if(!empty($q_string)){
                $person->bill_address = $q_string;   
            }
                   
        }
        if (Input::has('comment')) {
            $q_string = $request::get('comment');
            if(!empty($q_string)){
                $person->comment = $q_string;   
            }
                   
        }
        $person->save();
        Session::flash('success', 'A bejegyzés sikeresen módositva.');
        return Redirect::back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $persons = Persons::find($id);
        $persons->delete();
        Session::flash('success', 'Sikeresen törölve.');
        return Redirect::to('/motors'); 
    }
}
