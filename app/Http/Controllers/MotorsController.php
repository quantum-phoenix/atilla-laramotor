<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Motors;
use App\Persons;
use App\Pictures;

use DB;
use Input;
use Validator;
use Redirect;
use Session;
use Request;
use Response;
use Carbon\Carbon;
use DateTime;

class MotorsController extends Controller
{
    protected $translate;
    public function __construct(){
        $lang = array(
            "id"=>"Id",
            "image"=>"Kép",
            "year"=>"Év",
            "year_comment"=>"Év jegyzetek",
            "ordinal_number"=>"Sorszám",
            "date"=>"Dátum",
            "name"=>"Név",
            "name_comment"=>"Név jegyzet",
            "machine_type"=>"Gép fajta",
            "type"=>"Típus",
            "type_comment"=>"Típus jegyzet",
            "serial_number"=>"Gyáriszám",
            "serial_comment"=>"Gyáriszám jegyzet",
            "billing"=>"Számlázás",
            "total"=>"Végősszeg",
            "total_comment"=>"Végősszeg jegyzet",
            "repair"=>"Javítás",
            "repair_comment"=>"Javítás jegyzet",
            "earnings"=>"Tiszta",
            "earnings_comment"=>"Tiszta jegyzet",
            "expense"=>"Anyag",
            "expense_comment"=>"Anyag jegyzet",
            "iron_core_lenght"=>"Vasmaghosz",
            "iron_core_lenght_comment"=>"Vasmaghosz jegyzet",
            "iron_core_diameter"=>"Vasmag átmérő",
            "iron_core_diameter_comment"=>"Vasmag átmérő jegyzet",
            "swivel"=>"Forgorész",
            "kw"=>"Kw",
            "kw_comment"=>"Kw jegyzet",
            "pole"=>"Polus",
            "pole_comment"=>"Polus jegyzet",
            "volt"=>"Volt",
            "volt_comment"=>"Volt jegyzet",
            "amper"=>"Amper",
            "amper_comment"=>"Amper jegyzet",
            "stria"=>"Horony",
            "relay"=>"Relé",
            "relay_comment"=>"Relé jegyzet",
            "condenser"=>"Kondenzátor",
            "condenser_comment"=>"Kondenzátor jegyzet",
            "anchor"=>"Forgorész",
            "anchor_comment"=>"Forgorész jegyzet",
            "stator"=>"Álló",
            "stator_comment"=>"Álló jegyzet",
            "brake"=>"Fék",
            "is_there_serial_number"=>"Gyáriszám",
            "is_there_serial_number_comment"=>"Gyáriszám jegyzet",
            "y_or_d"=>"Y vagy D",
            "y_or_d_comment"=>"Y vagy D jegyzet",
            "wire"=>"Kötés",
            "wire_comment"=>"Kötés jegyzet",
            "turning"=>"Esztergálás",
            "turning_comment"=>"Esztergálás jegyzet",
            "component"=>"Alaktrész",
            "component_comment"=>"Alaktrész jegyzet",
            "envelope"=>"Átkötés",
            "pattern"=>"Sablon",
            "pattern_comment"=>"Sablon jegyzet",
            "pillow"=>"Csapágy",
            "pillow_comment"=>"Csapágy jegyzet",
            "seal"=>"Szimering",
            "seal_comment"=>"Szimering jegyzet",
            "slider_ring"=>"Csuszógyürü",
            "slider_ring_comment"=>"Csuszógyürü jegyzet",
            "coal_brush"=>"Szénkefe",
            "coal_brush_comment"=>"Szénkefe jegyzet",
            "is_there_draw"=>"Rajz vagy adat",
            "is_there_draw_comment"=>"Rajz vagy adat jegyzet",
            "kg"=>"Kg",
            "kg_comment"=>"Kg jegyzet",
            "primary_phase_resistor"=>"Fő fázis ellenállás",
            "primary_phase_resistor_comment"=>"Fő fázis ellenállás jegyzet",
            "secondary_phase_resistor"=>"Segéd fázis ellenállás",
            "secondary_phase_resistor_comment"=>"Segéd fázis ellenállás jegyzet",
            "empty_power"=>"Űresáram",
            "empty_power_comment"=>"Űresáram jegyzet",
            "standard_time"=>"Normaidő",
            "standard_time_comment"=>"Normaidő jegyzet",
            "view"=>"Megnéz"
            );
        $this->translate = $lang;
    }
    
    public function rnd_id($a) {
        if (Pictures::where('picture_id', '=', $a)->exists()) {
            $r = mt_rand(1, 2147483647);
            $this->rnd_id($r);
        }else{
            return $a;
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $ver = env('VERSION', '');
        $date = env('DATE', '');
        return view('pages.motors_index', compact('ver','date'));
    }

    public function database() {        

        $motors = Motors::orderBy('id', 'DESC')->paginate(50);
        $count = Motors::count();
        $lang = $this->translate;
        return view('pages.motors_database', compact('motors','count', 'lang'));        
    }

    public function search(Request $request) {
        $lang = $this->translate;
        $machine_type = Motors::select('machine_type')->groupBy('machine_type')->get();
        $billing = Motors::select('billing')->groupBy('billing')->get();
        $kw = Motors::select('kw')->groupBy('kw')->get();
        $pole = Motors::select('pole')->groupBy('pole')->get();
        $volt = Motors::select('volt')->groupBy('volt')->get();
        $amper = Motors::select('amper')->groupBy('amper')->get();
        $stria = Motors::select('stria')->groupBy('stria')->get();
        $y_or_d = Motors::select('y_or_d')->groupBy('y_or_d')->get();
        $wire = Motors::select('wire')->groupBy('wire')->get();
        $pattern = Motors::select('pattern')->groupBy('pattern')->get();
        $primary_phase_resistor = Motors::select('primary_phase_resistor')->groupBy('primary_phase_resistor')->get();
        $secondary_phase_resistor = Motors::select('secondary_phase_resistor')->groupBy('secondary_phase_resistor')->get();
        $empty_power = Motors::select('empty_power')->groupBy('empty_power')->get();
        $standard_time = Motors::select('standard_time')->groupBy('standard_time')->get();
        return view('pages.motors_search', compact('lang','machine_type','billing','kw','pole','volt','amper','stria','y_or_d','wire','pattern','primary_phase_resistor','secondary_phase_resistor','empty_power','standard_time'));
    }

    public function found(Request $request) {
        
        $q = Motors::query();

        if (Input::has('relay')) {
            $q_string = $request::get('relay');
            if($q_string == "true"){
                $q->where('relay', 1);
            }else if($q_string == "false"){
                $q->where('relay', 0);
            }        
        }
        if (Input::has('condenser')) {
            $q_string = $request::get('condenser');
            if($q_string == "true"){
                $q->where('condenser', 1);
            }else if($q_string == "false"){
                $q->where('condenser', 0);
            }        
        }
        if (Input::has('anchor')) {
            $q_string = $request::get('anchor');
            if($q_string == "true"){
                $q->where('anchor', 1);
            }else if($q_string == "false"){
                $q->where('anchor', 0);
            }        
        }
        if (Input::has('stator')) {
            $q_string = $request::get('stator');
            if($q_string == "true"){
                $q->where('stator', 1);
            }else if($q_string == "false"){
                $q->where('stator', 0);
            }        
        }
        if (Input::has('brake')) {
            $q_string = $request::get('brake');
            if($q_string == "true"){
                $q->where('brake', 1);
            }else if($q_string == "false"){
                $q->where('brake', 0);
            }        
        }
        if (Input::has('is_there_serial_number')) {
            $q_string = $request::get('is_there_serial_number');
            if($q_string == "true"){
                $q->where('is_there_serial_number', 1);
            }else if($q_string == "false"){
                $q->where('is_there_serial_number', 0);
            }        
        }
        if (Input::has('turning')) {
            $q_string = $request::get('turning');
            if($q_string == "true"){
                $q->where('turning', 1);
            }else if($q_string == "false"){
                $q->where('turning', 0);
            }        
        }
        if (Input::has('component')) {
            $q_string = $request::get('component');
            if($q_string == "true"){
                $q->where('component', 1);
            }else if($q_string == "false"){
                $q->where('component', 0);
            }        
        }  
        if (Input::has('envelope')) {
            $q_string = $request::get('envelope');
            if($q_string == "true"){
                $q->where('envelope', 1);
            }else if($q_string == "false"){
                $q->where('envelope', 0);
            }        
        }       
        if (Input::has('pillow')) {
            $q_string = $request::get('pillow');
            if($q_string == "true"){
                $q->where('pillow', 1);
            }else if($q_string == "false"){
                $q->where('pillow', 0);
            }        
        } 
        if (Input::has('seal')) {
            $q_string = $request::get('seal');
            if($q_string == "true"){
                $q->where('seal', 1);
            }else if($q_string == "false"){
                $q->where('seal', 0);
            }        
        }             
        if (Input::has('slider_ring')) {
            $q_string = $request::get('slider_ring');
            if($q_string == "true"){
                $q->where('slider_ring', 1);
            }else if($q_string == "false"){
                $q->where('slider_ring', 0);
            }        
        }
        if (Input::has('coal_brush')) {
            $q_string = $request::get('coal_brush');
            if($q_string == "true"){
                $q->where('coal_brush', 1);
            }else if($q_string == "false"){
                $q->where('coal_brush', 0);
            }        
        }
        if (Input::has('is_there_draw')) {
            $q_string = $request::get('is_there_draw');
            if($q_string == "true"){
                $q->where('is_there_draw', 1);
            }else if($q_string == "false"){
                $q->where('is_there_draw', 0);
            }        
        }
        if (Input::has('primary_phase_resistor')) {
            $q_string = $request::get('primary_phase_resistor');
            if($q_string == "true"){
                $q->where('primary_phase_resistor', 1);
            }else if($q_string == "false"){
                $q->where('primary_phase_resistor', 0);
            }        
        }
        if (Input::has('secondary_phase_resistor')) {
            $q_string = $request::get('secondary_phase_resistor');
            if($q_string == "true"){
                $q->where('secondary_phase_resistor', 1);
            }else if($q_string == "false"){
                $q->where('secondary_phase_resistor', 0);
            }        
        }
        if (Input::has('empty_power')) {
            $q_string = $request::get('empty_power');
            if($q_string == "true"){
                $q->where('empty_power', 1);
            }else if($q_string == "false"){
                $q->where('empty_power', 0);
            }        
        }
        if (Input::has('standard_time')) {
            $q_string = $request::get('standard_time');
            if($q_string == "true"){
                $q->where('standard_time', 1);
            }else if($q_string == "false"){
                $q->where('standard_time', 0);
            }        
        }
        if (Input::has('from_year') and Input::has('from_month') and  Input::has('from_day') and Input::has('to_year') and Input::has('to_month') and Input::has('to_day')) {
            $from_year = $request::get('from_year');
            $from_month = $request::get('from_month');
            $from_day = $request::get('from_day'); 
            $to_year = $request::get('to_year');          
            $to_month = $request::get('to_month');
            $to_day = $request::get('to_day');

            $from_date = Carbon::createFromDate(intval($from_year), intval($from_month), intval($from_day));
            $to_date = Carbon::createFromDate(intval($to_year), intval($to_month), intval($to_day));
            $q->whereBetween('date', array($from_date, $to_date));
        }
        if (Input::has('name')) {
            $q_string = $request::get('name');
            if(!empty($q_string)){
                $row = Persons::where('name', '=', $q_string)->first();                
                $id = $row->id;
                $q->where('person_id','=',$id);
            }            
        }
        if (Input::has('machine_type')) {
            $q_string = $request::get('machine_type');
            if($q_string != "null"){
                $q->where('machine_type', 'like', "%$q_string%");
            }
        }
        if (Input::has('type')) {
            $q_string = $request::get('type');
            if(!empty($q_string)){
                $q->where('type', 'like', "%$q_string%");
            }      
        }
        if (Input::has('serial_number')) {
            $q_string = $request::get('serial_number');
            if(!empty($q_string)){
                $q->where('serial_number', 'like', "%$q_string%");
            }
        }
        if (Input::has('billing')) {
            $q_string = $request::get('billing');
            if($q_string != "null"){
                $q->where('billing', 'like', "%$q_string%");
            }
        }
        if (Input::has('total')) {
            $q_string = $request::get('total');
            if(!empty($q_string)){
                $q->where('total', 'like', "%$q_string%");
            }
        }
        if (Input::has('repair')) {
            $q_string = $request::get('repair');
            if(!empty($q_string)){
                $q->where('repair', 'like', "%$q_string%");
            }
        }
        if (Input::has('earnings')) {
            $q_string = $request::get('earnings');
            if(!empty($q_string)){
                $q->where('earnings', 'like', "%$q_string%");
            }
        }
        if (Input::has('expense')) {
            $q_string = $request::get('expense');
            if(!empty($q_string)){
                $q->where('expense', 'like', "%$q_string%");
            }
        }
        if (Input::has('iron_core_lenght')) {
            $q_string = $request::get('iron_core_lenght');
            if(!empty($q_string)){
                $q->where('iron_core_lenght', 'like', "%$q_string%");
            }
        }
        if (Input::has('iron_core_diameter')) {
            $q_string = $request::get('iron_core_diameter');
            if(!empty($q_string)){
                $q->where('iron_core_diameter', 'like', "%$q_string%");
            }
        }
        if (Input::has('swivel')) {
            $q_string = $request::get('swivel');
            if(!empty($q_string)){
                $q->where('swivel', 'like', "%$q_string%");
            }
        }
        if (Input::has('kw')) {
            $q_string = $request::get('kw');
            if($q_string != "null"){
                $q->where('kw', 'like', "%$q_string%");
            }
        }
        if (Input::has('pole')) {
            $q_string = $request::get('pole');
            if($q_string != "null"){
                $q->where('pole', 'like', "%$q_string%");
            }
        }
        if (Input::has('volt')) {
            $q_string = $request::get('volt');
            if($q_string != "null"){
                $q->where('volt', 'like', "%$q_string%");
            }
        }
        if (Input::has('amper')) {
            $q_string = $request::get('amper');
            if($q_string != "null"){
                $q->where('amper', 'like', "%$q_string%");
            }
        }
        if (Input::has('stria')) {
            $q_string = $request::get('stria');
            if($q_string != "null"){
                $q->where('stria', 'like', "%$q_string%");
            }
        }
        if (Input::has('y_or_d')) {
            $q_string = $request::get('y_or_d');
            if($q_string != "null"){
                $q->where('y_or_d', 'like', "%$q_string%");
            }
        }
        if (Input::has('wire')) {
            $q_string = $request::get('wire');
            if($q_string != "null"){
                $q->where('wire', 'like', "%$q_string%");
            }
        }
        if (Input::has('pattern')) {
            $q_string = $request::get('pattern');
            if($q_string != "null"){
                $q->where('pattern', 'like', "%$q_string%");
            }
        }
        if (Input::has('kg')) {
            $q_string = $request::get('kg');
            if(!empty($q_string)){
                $q->where('kg', 'like', "%$q_string%");
            }
        }
        if (Input::has('ordinal_number')) {
            $q_string = $request::get('ordinal_number');
            if(!empty($q_string)){
                $q->where('ordinal_number', 'like', "%$q_string%");
            }
        }
        $count = $q->get()->count();
        $motors = $q->orderBy('id', 'DESC')->paginate(14);   
        $motors->load('person'); 
        $motors->load('picture');
        $lang = $this->translate;
        //dd($motors);
        return view('pages.motors_found', compact('motors', 'count', 'lang'));
        
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $lang = $this->translate;
        $machine_type = Motors::select('machine_type')->groupBy('machine_type')->get();
        $billing = Motors::select('billing')->groupBy('billing')->get();
        $kw = Motors::select('kw')->groupBy('kw')->get();
        $pole = Motors::select('pole')->groupBy('pole')->get();
        $volt = Motors::select('volt')->groupBy('volt')->get();
        $amper = Motors::select('amper')->groupBy('amper')->get();
        $stria = Motors::select('stria')->groupBy('stria')->get();
        $y_or_d = Motors::select('y_or_d')->groupBy('y_or_d')->get();
        $wire = Motors::select('wire')->groupBy('wire')->get();
        $pattern = Motors::select('pattern')->groupBy('pattern')->get();
        $primary_phase_resistor = Motors::select('primary_phase_resistor')->groupBy('primary_phase_resistor')->get();
        $secondary_phase_resistor = Motors::select('secondary_phase_resistor')->groupBy('secondary_phase_resistor')->get();
        $empty_power = Motors::select('empty_power')->groupBy('empty_power')->get();
        $standard_time = Motors::select('standard_time')->groupBy('standard_time')->get();   
        return view('pages.motors_insert', compact('lang','machine_type','billing','kw','pole','volt','amper','stria','y_or_d','wire','pattern','primary_phase_resistor','secondary_phase_resistor','empty_power','standard_time'));      
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $rules = array(
            'image' => 'mimes:jpeg,jpg,bmp,png',
            'expense' => 'integer',
            'earnings' => 'integer',
            'total' => 'integer',
            'name' => 'required',
        );
        $messages = array(
            'mimes' => 'A feltöltetni kívánt kép nem felel meg a kritériumoknak. (Ilyen lehet a kép kiterjesztése: jpeg, jpg, bmp vagy png. A kép se lehet bármekkora.)',
            'integer'  => 'A beírt szőveg nem szám.',
            'required' => 'Ennek a mezőnek a kitőltése kötelező.',
        );

        $validator = Validator::make(Input::all(), $rules, $messages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::back()->withInput()->withErrors($validator);

        }

        $motor = new Motors;        
        
        if (Input::hasFile('images')) {
            $files = $request::file('images');
            $file_count = count($files);
            //dd($files);
            $uploadcount = 0;
            $r = mt_rand(1, 2147483647);
            //dd($r);
            $picture_id = $this->rnd_id($r);
            
            $destinationPath = 'uploads';
            foreach($files as $file) {
                if ($file->isValid()) {                   
                    $extension = $file->getClientOriginalExtension();
                    $fileName = date('Y-m-d-H-i-s') .'-'. rand(11111, 99999) . '.' . strtolower("{$extension}");
                    $picture = new Pictures;
                    $picture->picture_file_name = $fileName;
                    $picture->picture_id = $picture_id;
                    $picture->save();
                    $row = Pictures::where('picture_file_name', $fileName)->first();
                    $motor->picture_id = $row->picture_id;
                    $file->move($destinationPath, $fileName);
                    $uploadcount ++;                    
                } 
                else {
                    Session::flash('image-error', 'A feltöltetni kívánt kép nem megfelelő. (Valószínűleg túl nagy.)');
                    return redirect()->back()->withInput()->withErrors();
                }
            }
            if($uploadcount == $file_count){
                  Session::flash('image', 'A képek feltöltése sikeres.'); 
            } 
            else {
                  return redirect()->back()->withInput()->withErrors($validator);
            }
            
        } 
        
        /*
        if (Input::has('name')) {
            $q_string = $request::get('name');
            if(!empty($q_string)){
                $row = Persons::where('name', $q_string)->first();
                if(is_null($row)){
                    Session::flash('success', 'Olyan nevet akarsz hozzáadni ami nem is létezik.');
                    return Redirect::to('/motors');
                }else{
                    $motor->person_id = $row->id; 
                }
            }
        }*/

        if (Input::has('relay')) {
            $q_string = $request::get('relay');
            if($q_string == "true"){
                $motor->relay = 1;
            }else if($q_string == "false"){
                $motor->relay = 0;
            }        
        }
        if (Input::has('condenser')) {
            $q_string = $request::get('condenser');
            if($q_string == "true"){
                $motor->condenser = 1;
            }else if($q_string == "false"){
                $motor->condenser = 0;
            }        
        }
        if (Input::has('anchor')) {
            $q_string = $request::get('anchor');
            if($q_string == "true"){
                $motor->anchor = 1;
            }else if($q_string == "false"){
                $motor->anchor = 0;
            }        
        }
        if (Input::has('stator')) {
            $q_string = $request::get('stator');
            if($q_string == "true"){
                $motor->stator = 1;
            }else if($q_string == "false"){
                $motor->stator = 0;
            }        
        }
        if (Input::has('brake')) {
            $q_string = $request::get('brake');
            if($q_string == "true"){
               $motor->brake = 1;
            }else if($q_string == "false"){
                $motor->brake = 0;
            }        
        }
        if (Input::has('is_there_serial_number')) {
            $q_string = $request::get('is_there_serial_number');
            if($q_string == "true"){
                $motor->is_there_serial_number = 1;
            }else if($q_string == "false"){
                $motor->is_there_serial_number = 0;
            }        
        }
        if (Input::has('turning')) {
            $q_string = $request::get('turning');
            if($q_string == "true"){
                $motor->turning = 1;
            }else if($q_string == "false"){
                $motor->turning = 0;
            }        
        }
        if (Input::has('component')) {
            $q_string = $request::get('component');
            if($q_string == "true"){
                $motor->component = 1;
            }else if($q_string == "false"){
                $motor->component = 0;
            }        
        }  
        if (Input::has('envelope')) {
            $q_string = $request::get('envelope');
            if($q_string == "true"){
                $motor->envelope = 1;
            }else if($q_string == "false"){
                $motor->envelope = 0;
            }        
        }       
        if (Input::has('pillow')) {
            $q_string = $request::get('pillow');
            if($q_string == "true"){
                $motor->pillow = 1;
            }else if($q_string == "false"){
                $motor->pillow = 0;
            }        
        } 
        if (Input::has('seal')) {
            $q_string = $request::get('seal');
            if($q_string == "true"){
                $motor->seal = 1;
            }else if($q_string == "false"){
                $motor->seal = 0;
            }        
        }             
        if (Input::has('slider_ring')) {
            $q_string = $request::get('slider_ring');
            if($q_string == "true"){
                $motor->slider_ring = 1;
            }else if($q_string == "false"){
                $motor->slider_ring = 0;
            }        
        }
        if (Input::has('coal_brush')) {
            $q_string = $request::get('coal_brush');
            if($q_string == "true"){
                $motor->coal_brush = 1;
            }else if($q_string == "false"){
                $motor->coal_brush = 0;
            }        
        }
        if (Input::has('is_there_draw')) {
            $q_string = $request::get('is_there_draw');
            if($q_string == "true"){
                $motor->is_there_draw = 1;
            }else if($q_string == "false"){
                $motor->is_there_draw = 0;
            }        
        }
        if (Input::has('primary_phase_resistor')) {
            $q_string = $request::get('primary_phase_resistor');
            if($q_string == "true"){
                $motor->primary_phase_resistor = 1;
            }else if($q_string == "false"){
                $motor->primary_phase_resistor = 0;
            }        
        }
        if (Input::has('secondary_phase_resistor')) {
            $q_string = $request::get('secondary_phase_resistor');
            if($q_string == "true"){
                $motor->secondary_phase_resistor = 1;
            }else if($q_string == "false"){
                $motor->secondary_phase_resistor = 0;
            }        
        }
        if (Input::has('empty_power')) {
            $q_string = $request::get('empty_power');
            if($q_string == "true"){
                $motor->empty_power = 1;
            }else if($q_string == "false"){
                $motor->empty_power = 0;
            }        
        }
        if (Input::has('standard_time')) {
            $q_string = $request::get('standard_time');
            if($q_string == "true"){
                $motor->standard_time = 1;
            }else if($q_string == "false"){
                $motor->standard_time = 0;
            }        
        }
        if (Input::has('selected_year') and Input::has('selected_month') and Input::has('selected_day')) {
            $year = $request::get('selected_year');
            $month = $request::get('selected_month');
            $day = $request::get('selected_day');
            if(!empty($year) and !empty($month) and !empty($day)){
                $d = $year . '-' . $month . '-' . $day;
                $motor->date = $d;   
            }
        }

        if (Input::has('name')) {
            $q_string = $request::get('name');
            if(!empty($q_string)){
                $row = Persons::where('name', $q_string)->first();
                if(is_null($row)){
                    Session::flash('success', 'Olyan nevet akarsz hozzáadni ami nem is létezik.');
                    return Redirect::to('/motors');
                }else{
                    $motor->person_id = $row->id; 
                }
            }
        }
        if (Input::has('machine_type')) {
            $q_string = $request::get('machine_type');
            if($q_string != "null"){
                $motor->machine_type = $q_string;
            }
        }
        if (Input::has('type')) {
            $q_string = $request::get('type');
            if(!empty($q_string)){
                $motor->type = $q_string;   
            }
                   
        }
        if (Input::has('serial_number')) {
            $q_string = $request::get('serial_number');
            if(!empty($q_string)){
                $motor->serial_number = $q_string;   
            }
        }
        if (Input::has('billing')) {
            $q_string = $request::get('billing');
            if($q_string != "null"){
                $motor->billing = $q_string;
            }
        }
        if (Input::has('total')) {
            $q_string = $request::get('total');
            if(!empty($q_string)){
                $motor->total = $q_string;   
            }
        }
        if (Input::has('repair')) {
            $q_string = $request::get('repair');
            if(!empty($q_string)){
                $motor->repair = $q_string;   
            }
        }
        if (Input::has('earnings')) {
            $q_string = $request::get('earnings');
            if(!empty($q_string)){
                $motor->earnings = $q_string;   
            }
        }
        if (Input::has('expense')) {
            $q_string = $request::get('expense');
            if(!empty($q_string)){
                $motor->expense = $q_string;   
            }
        }
        if (Input::has('iron_core_lenght')) {
            $q_string = $request::get('iron_core_lenght');
            if(!empty($q_string)){
                $motor->iron_core_lenght = $q_string;   
            }
        }
        if (Input::has('iron_core_diameter')) {
            $q_string = $request::get('iron_core_diameter');
            if(!empty($q_string)){
                $motor->iron_core_diameter = $q_string;   
            }
        }
        if (Input::has('swivel')) {
            $q_string = $request::get('swivel');
            if(!empty($q_string)){
                $motor->swivel = $q_string;   
            }
        }
        if (Input::has('kw')) {
            $q_string = $request::get('kw');
            if(!empty($q_string)){
                $motor->kw = $q_string;   
            }
        }
        if (Input::has('pole')) {
            $q_string = $request::get('pole');
            if(!empty($q_string)){
                $motor->pole = $q_string;   
            }
        }
        if (Input::has('volt')) {
            $q_string = $request::get('volt');
            if(!empty($q_string)){
                $motor->volt = $q_string;   
            }
        }
        if (Input::has('amper')) {
            $q_string = $request::get('amper');
            if(!empty($q_string)){
                $motor->amper = $q_string;   
            }
        }
        if (Input::has('stria')) {
            $q_string = $request::get('stria');
            if(!empty($q_string)){
                $motor->stria = $q_string;   
            }
        }
        if (Input::has('y_or_d')) {
            $q_string = $request::get('y_or_d');
            if(!empty($q_string)){
                $motor->y_or_d = $q_string;   
            }
        }
        if (Input::has('wire')) {
            $q_string = $request::get('wire');
            if(!empty($q_string)){
                $motor->wire = $q_string;   
            }
        }
        if (Input::has('pattern')) {
            $q_string = $request::get('pattern');
            if(!empty($q_string)){
                $motor->pattern = $q_string;   
            }
        }
        if (Input::has('kg')) {
            $q_string = $request::get('kg');
            if(!empty($q_string)){
                $motor->kg = $q_string;   
            }
        }
        if (Input::has('ordinal_number')) {
            $q_string = $request::get('ordinal_number');
            if(!empty($q_string)){
                $motor->ordinal_number = $q_string;   
            }
        }     
        $motor->save();
        Session::flash('success', 'Az új bejegyzés sikeresen hozzáadva.');
        return Redirect::to('motors/create'); 
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $motors = Motors::find($id);        
        $lang = $this->translate;
        return view('pages.motors_show', compact('motors', 'lang'));      
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $motors = Motors::find($id);
        $lang = $this->translate;       
        return view('pages.motors_edit', compact('motors', 'lang'));        
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        $rules = array(
            'image'             => 'mimes:jpeg,jpg,bmp,png',
            'expense'            => 'integer',
            'earnings'            => 'integer',
            'total'            => 'integer',
        );
        $messages = array(
            'mimes' => 'A feltöltetni kívánt kép nem felel meg a kritériumoknak. (Ilyen lehet a kép kiterjesztése: jpeg, jpg, bmp vagy png. A kép se lehet bármekkora.)',
            'integer'  => 'A beírt szőveg nem szám.'
        );

        $validator = Validator::make(Input::all(), $rules, $messages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::back()->withInput()->withErrors($validator);

        }

        $motor = Motors::find($id);
        if (Input::has('year')) {
            $q_string = $request::get('year');
            if(!empty($q_string)){
                $motor->year = $q_string;   
            }
                   
        }
        if (Input::has('year_comment')) {
            $q_string = $request::get('year_comment');
            if(!empty($q_string)){
                $motor->year_comment = $q_string;   
            }
                   
        }
        if (Input::has('date')) {
            $q_string = $request::get('date');
            if(!empty($q_string)){
                $motor->date = $q_string;   
            }
                   
        }
        if (Input::has('name')) {
            $q_string = $request::get('name');
            if(!empty($q_string)){
                $motor->name = $q_string;   
            }
                   
        }
        if (Input::has('name_comment')) {
            $q_string = $request::get('name_comment');
            if(!empty($q_string)){
                $motor->name_comment = $q_string;   
            }
                   
        }
        if (Input::has('machine_type')) {
            $q_string = $request::get('machine_type');
            if(!empty($q_string)){
                $motor->machine_type = $q_string;   
            }
                   
        }
        if (Input::has('type')) {
            $q_string = $request::get('type');
            if(!empty($q_string)){
                $motor->type = $q_string;   
            }
                   
        }
        $motor->save();
        Session::flash('success', 'A bejegyzés sikeresen módositva.');
        return Redirect::back();
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $motors = Motors::find($id);
        $motors->delete();
        Session::flash('success', 'Sikeresen törölve.');
        return Redirect::to('/motors');       
    }
}
