<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Motors;

use Input;
use Request;

class PrototypeController extends Controller
{
    public function names(Request $request) {
        $select_string = 'name';
        
        //$res = Motors::select($select_string)->get();
        //$res = Motors::find(1)->person()->where('name', 'Herman Ottó')->get();
        //$res = Motors::get()->person->name;
        $res = Motors::all();
        $res->load('person');
        //dd($res[0]);
        return view('pages.prototype', compact('res'));
    }
}
