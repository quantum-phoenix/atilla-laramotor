<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Persons;

use Input;
use Request;
use Response;

class AutocompleteController extends Controller
{
	public function autocomplete_name(Request $request) {
        $q = Persons::query();
        $results = array();
        if (Input::has('name')) {
            $name = $request::get('name');
            $q->where('name', 'like', "%$name%");
        }
        $names = $q->get();
        foreach ($names as $name)
        {
            $results[] = [ 'name' => $name->name , 'id' => $name->id];
        }
        return Response::json($results);
    }
}