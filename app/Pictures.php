<?php
namespace App;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Pictures extends Eloquent
{
    protected $table = 'pictures';
    public function motors(){
        return $this->belongsTo('App\Motors');
    }    
}