<?php
namespace App;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Carbon\Carbon;
use App\Persons;
use DB;

class Motors extends Eloquent
{
    protected $table = 'motors';
    protected $dates = array('date');    
    public function setDateAttribute($date) {
        $this->attributes['date'] = Carbon::createFromFormat('Y-m-d', $date);
    }
    public function getDateAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d');
    }
    public function person(){
        //return $this->hasMany('App\Persons', 'id');
        return $this->belongsTo('App\Persons');
    } 
    public function picture(){
        return $this->hasMany('App\Pictures', 'picture_id', 'picture_id');
    }
}