<?php
namespace App;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Persons extends Eloquent
{
    protected $table = 'persons';
    public function motors(){
        return $this->hasMany('App\Motors');
    }    
}
